<?php

namespace App\Exceptions;

use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Throwable;

class Handler extends ExceptionHandler
{
    /**
     * A list of exception types with their corresponding custom log levels.
     *
     * @var array<class-string<\Throwable>, \Psr\Log\LogLevel::*>
     */
    protected $levels = [
        //
    ];

    /**
     * A list of the exception types that are not reported.
     *
     * @var array<int, class-string<\Throwable>>
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed to the session on validation exceptions.
     *
     * @var array<int, string>
     */
    protected $dontFlash = [
        'current_password',
        'password',
        'password_confirmation',
    ];

    /**
     * Register the exception handling callbacks for the application.
     *
     * @return void
     */
    public function register()
    {
        $this->reportable(function (Throwable $e) {
            
        });
        // $this->renderable(function (InvalidOrderException $e, $request) {
        //     return response()->view('index', [], 500);
        // });
        
    }
    public function render($request, Throwable $exception)
    {
        // $atas=$this->Header($request);
        // $header=$atas[0];

        if ($this->isHttpException($exception)) {
            if ($exception->getStatusCode() == 404) {
                return redirect('/');
            }

            if ($exception->getStatusCode() == 500) {
                //dd("test");
                return redirect('/');
            }
        }
        // dd("asdsad");
        return parent::render($request, $exception);
    }
    
}
