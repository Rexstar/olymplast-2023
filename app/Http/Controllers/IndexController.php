<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
class IndexController extends Controller
{
    public function index()
    {
        $banner_mobile = db::table('uni_banners as b')
                        ->where('b.uni_gen_applications_id',3)
                        ->where('b.name','home')
                        ->where('b.status',1)
                        ->where(function($q) {
                            $q->where('b.duration_start','<=',now())
                            ->orWhereNull('b.duration_start');
                        })
                        ->where(function($q) {
                            $q->where('b.duration_end','>=',now())
                            ->orWhereNull('b.duration_end');
                        })
                        ->whereNotNull('image_desktop')
                        ->orderBy('order_data','ASC')
                        ->get();
        
        $banner_pc = db::table('uni_banners as b')
                        ->where('b.uni_gen_applications_id',3)
                        ->where('b.name','home')
                        ->where('b.status',1)
                        ->where(function($q) {
                            $q->where('b.duration_start','<=',now())
                            ->orWhereNull('b.duration_start');
                        })
                        ->where(function($q) {
                            $q->where('b.duration_end','>=',now())
                            ->orWhereNull('b.duration_end');
                        })
                        ->whereNotNull('image_desktop')
                        ->orderBy('order_data','ASC')
                        ->get();
        $meta=db::table('uni_pages')
                        ->where('uni_gen_applications_id',3)
                        ->where('status',1)
                        ->where('page_name',"Home")
                        ->orderBy('id','ASC')
                        ->first();
        $kategori=db::table('uni_categories')
                        ->where('uni_gen_applications_id',3)
                        ->where('status',1)
                        ->orderBy('id','ASC')
                        ->get();
        $terbaru=db::table('uni_product_filters as pf')
                        ->join('brands_olymplast_products_view as pv','pv.id','=','pf.uni_products_id')
                        ->where('pf.uni_gen_applications_id',3)
                        ->where('pf.status',1)
                        ->where('pf.filter_name','terlaris')
                        ->orderBy('pf.order_no','DESC')
                        ->limit(10)
                        ->get();
        $ruangan=db::table('uni_inspirations as i') 
                        ->join('uni_inspiration_applications as ia','i.id','=','ia.uni_inspirations_id')
                        ->where('ia.uni_gen_applications_id',2)
                        ->where('i.status',1)
                        ->where('i.highlight',1)
                        ->limit(4)
                        ->get();
        // $kol=db::table('uni_product_images')
        //                 ->select('id','title','alt_text','filename_webp')
        //                 ->where('no',1)
        //                 ->where('status',1)
        //                 ->where('type','=','KOL')
        //                 ->orderBy('no','DESC')
        //                 ->limit(4)
        //                 ->offset(1)
        //                 ->get();
        // $kol1=db::table('uni_product_images')
        //                 ->select('id','title','alt_text','filename_webp')
        //                 ->where('no',1)
        //                 ->where('status',1)
        //                 ->where('type','=','KOL')
        //                 ->orderBy('no','DESC')
        //                 ->first();
        $news=DB::table('brands_olymplast_news_list_view')
                        ->distinct('id')
                        ->orderby('id','DESC')
                        ->orderby('date','DESC')
                        ->limit(6)
                        ->get();
        $newsg=DB::table('uni_news as n')
                        ->join('uni_news_applications as a','a.uni_news_id','=','n.id')
                        ->where('a.uni_gen_applications_id',2)
                        ->where('n.status',1)
                        ->orderby('n.created_at','DESC')
                        ->limit(3)
                        ->get();
        $kol=db::table('brands_endorsements')
                        // ->select('id','title','alt_text','filename_webp')
                        ->where('status',1)
                        ->where('brand','=','olymplast')
                        ->orderBy('order_data','DESC')
                        ->offset(1)
                        ->get();
        $first=db::table('brands_endorsements')
                        // ->select('id','title','alt_text','filename_webp')
                        ->where('status',1)
                        ->where('brand','=','olymplast')
                        ->orderBy('order_data','DESC')
                        ->first();
        $windowWidth = $this->getWindowWidth();
        session(['windowWidth' => $windowWidth]);
                    
        //dd($windowWidth);
        return view('index', [
            "meta" => $meta,
            "banner_mobile" => $banner_mobile,
            "banner_pc" => $banner_pc,
            "kategori" => $kategori,
            "terbaru" => $terbaru,
            "ruangan" => $ruangan,
            "news" => $news,
            "kol" => $kol,
            "first" => $first,
            "windowWidth" => $windowWidth,
        ]);
    }
    private function getWindowWidth()
    {
        // Mendapatkan lebar layar pada sisi server
        $userAgent = request()->server('HTTP_USER_AGENT');
        if (strpos($userAgent, 'Mobile') !== false) {
            // Jika perangkat mobile
            return 0; 
        } else {
            // Jika bukan perangkat mobile
            return 1; 
        }
    }
    public function kol(Request $request)
    {
       
        //dd($banner);
        $meta=db::table('uni_pages')
        ->where('uni_gen_applications_id',3)
        ->where('status',1)
        ->where('page_name',"KOL")
        ->orderBy('id','ASC')
        ->first();
        $slug=db::table('uni_products')
                    ->select('id','slug')
                    ->get();
        $about2=db::table('uni_single as a')
                    ->where('a.uni_gen_applications_id',2)
                    ->where('name','about2')
                    ->where('status',1)
                    ->first();

        $fitur=db::table('uni_product_images')
            ->select('id','title','alt_text','filename_webp')
            ->where('status',1)
            ->where('type','=','KOL')
            ->orderBy('no','DESC')
            ->paginate(12);
        $kol=db::table('brands_endorsements')
            // ->select('id','title','alt_text','filename_webp')
            ->where('status',1)
            ->where('brand','=','olymplast')
            ->orderBy('order_data','DESC')
            ->get();
        //dd($kol);
        return view('kol',["fitur"=>$fitur,"slug"=>$slug,"kol"=>$kol,"meta"=>$meta]);
    }
    public function katalog()
    {

        return response()->download('Katalog-Olymplast.pdf');

    }
}
