<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use DB;
use Response;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\URL;


use Illuminate\Pagination\Paginator;
use Illuminate\Support\Collection;
use Illuminate\Pagination\LengthAwarePaginator;

class MicroSiteController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */


     public function springbed(Request $request)
     {
         $banner=db::table('uni_banners as a')
                     ->join('uni_banner_applications as b','a.id','=','b.uni_banners_id')
                     ->where('a.name','tentang')
                     ->where('a.status',1)
                     ->where('b.uni_gen_applications_id',2)
                     ->first();
         //dd($banner);
         $meta=db::table('uni_pages')
         ->where('uni_gen_applications_id',2)
         ->where('status',1)
         ->where('page_name',"Springbed Olympic")
         ->orderBy('id','ASC')
         ->first();
         $slug=db::table('uni_products')
                     ->select('id','slug')
                     ->get();
         $about2=db::table('uni_single as a')
                     ->where('a.uni_gen_applications_id',2)
                     ->where('name','about2')
                     ->where('status',1)
                     ->first();
 
 
         $fitur=db::table('uni_product_superiorities  as s')
         ->select('s.uni_products_id','s.name','s.image')
         ->whereNotNull('s.image')
         ->get();
         //dd($slug);
         return view('springbed',["fitur"=>$fitur,"slug"=>$slug,"banner"=>$banner,"meta"=>$meta]);
     }
    public function kasur_busa(Request $request)
    {
        $banner=db::table('uni_banners as a')
                    ->join('uni_banner_applications as b','a.id','=','b.uni_banners_id')
                    ->where('a.name','tentang')
                    ->where('a.status',1)
                    ->where('b.uni_gen_applications_id',2)
                    ->first();
        //dd($banner);
        $meta=db::table('uni_pages')
        ->where('uni_gen_applications_id',2)
        ->where('status',1)
        ->where('page_name',"Springbed Olympic")
        ->orderBy('id','ASC')
        ->first();
        $slug=db::table('uni_products')
                    ->select('id','slug')
                    ->get();
        $about2=db::table('uni_single as a')
                    ->where('a.uni_gen_applications_id',2)
                    ->where('name','about2')
                    ->where('status',1)
                    ->first();


        $varian=db::table('uni_product_images  as s')
        ->select('s.uni_products_id','s.filename_webp')
        ->where('s.no',1)
        ->orderby('s.uni_products_id')
        ->get();
        //dd($varian);
        return view('kasurbusa',["varian"=>$varian,"slug"=>$slug,"banner"=>$banner,"meta"=>$meta]);
    }
    public function kol(Request $request)
    {
       
        //dd($banner);
        $meta=db::table('uni_pages')
        ->where('uni_gen_applications_id',2)
        ->where('status',1)
        ->where('page_name',"Springbed Olympic")
        ->orderBy('id','ASC')
        ->first();
        $slug=db::table('uni_products')
                    ->select('id','slug')
                    ->get();
        $about2=db::table('uni_single as a')
                    ->where('a.uni_gen_applications_id',2)
                    ->where('name','about2')
                    ->where('status',1)
                    ->first();

        $fitur=db::table('uni_product_images')
            ->select('id','title','alt_text','filename_webp')
            ->where('status',1)
            ->where('type','=','KOL')
            ->orderBy('no','DESC')
            ->paginate(12);
        $kol=db::table('brands_endorsements')
            // ->select('id','title','alt_text','filename_webp')
            ->where('status',1)
            ->where('brand','=','olymplast')
            ->orderBy('order_data','DESC')
            ->get();
        //dd($kol);
        return view('kol',["fitur"=>$fitur,"slug"=>$slug,"kol"=>$kol,"meta"=>$meta]);
    }
    public function series(Request $request)
    {
        $banner=db::table('uni_banners as a')
                    ->join('uni_banner_applications as b','a.id','=','b.uni_banners_id')
                    ->where('a.name','tentang')
                    ->where('a.status',1)
                    ->where('b.uni_gen_applications_id',2)
                    ->first();
        //dd($banner);
        $meta=db::table('uni_pages')
        ->where('uni_gen_applications_id',2)
        ->where('status',1)
        ->where('page_name',"Springbed Olympic")
        ->orderBy('id','ASC')
        ->first();
        $slug=db::table('uni_products')
                    ->select('id','slug')
                    ->get();
        $produk = db::table('brands_olympic_products_view')
                    ->where('series',$request->series)
                    ->orderby('id','DESC')
                    ->paginate(12);
        $parent_cat = db::table('uni_categories')
                    ->where('uni_gen_applications_id',2)
                    ->whereNull('uni_categories_id')
                    ->get();
        $child_cat = db::table('uni_categories')
                    ->where('uni_gen_applications_id',2)
                    ->whereNotNull('uni_categories_id')
                    ->get();
        $series = db::table('brands_olympic_products_view')
                    ->select('series')
                    ->whereNotNull('series')
                    ->groupby('series')
                    ->get();
        
        return view('series',["series"=>$series,"parent_cat"=>$parent_cat,"child_cat"=>$child_cat,"produk"=>$produk,"slug"=>$slug,"banner"=>$banner,"meta"=>$meta]);
    }
    
    public function fitur(Request $request)
    {
        $banner=db::table('uni_banners as a')
                    ->join('uni_banner_applications as b','a.id','=','b.uni_banners_id')
                    ->where('a.name','fitur')
                    ->where('a.status',1)
                    ->where('b.uni_gen_applications_id',2)
                    ->first();
        //dd($banner);
        $meta=db::table('uni_pages')
        ->where('uni_gen_applications_id',2)
        ->where('status',1)
        ->where('page_name',"Springbed Olympic")
        ->orderBy('id','ASC')
        ->first();
        $slug=db::table('uni_products')
                    ->select('id','slug')
                    ->get();

        $fitur=db::table('uni_features')
        ->where('uni_gen_applications_id',3)
        ->get();
        $category_fitur=db::table('uni_features')
        ->select('category')
        ->where('uni_gen_applications_id',3)
        ->groupBy('category')
        ->get();
        //dd($category_fitur);
        return view('fitur',["fitur"=>$fitur,"slug"=>$slug,"banner"=>$banner,"meta"=>$meta,"category_fitur"=>$category_fitur]);
    }

    public function paginate($items, $perPage = 12, $page = null, $options = [])
    {
        $page = $page ?: (Paginator::resolveCurrentPage() ?: 1);
        $items = $items instanceof Collection ? $items : Collection::make($items);
        return new LengthAwarePaginator($items->forPage($page, $perPage), $items->count(), $perPage, $page, $options);
    }
    public function search(Request $request)
    {

        $banner=db::table('uni_banners')
        ->where('name','pencarian')
        ->where('status',1)
        ->first();
        // $produks=db::table('uni_product_olympic_view')
        //         ->where('uni_product_olympic_view.name','ILIKE','%'.$request->cari.'%')
        //         ->groupBy('uni_product_olympic_view.id')
        //         ->select('uni_product_olympic_view.id')
        //         ->paginate(8);
        if(isset($request->cari)){
            DB::table('uni_search_history')->insert([
                'uni_gen_applications_id' => 7,
                'text' => $request->cari,
                'ip' => request()->ip()
            ]);
        }


        $produks = db::table('uni_product_olympic_view as p')
                ->select('p.id')
                ->where('p.name','ILIKE','%'.$request->cari.'%')
                ->orderby('id','DESC')
                ->paginate(8);
        $total_produks = db::table('uni_product_olympic_view as p')
                ->select('p.id')
                ->where('p.name','ILIKE','%'.$request->cari.'%')
                ->get();
        $total_produkss = $total_produks->count(); 
         //dd($total_produk);
        $list=[];
        foreach($produks as $pr)
        {
            $detail=db::table('uni_product_olympic_view')
                    ->where('id',$pr->id)
                    ->get();

            array_push($list,$detail[0]);
        }
        $list_p=[];
        //dd($list);
        foreach($list as $p)
        {
            $var_img=db::table('uni_products as pr')
                    ->where('pr.id',$p->id)
                    ->leftjoin('uni_product_images as pi','pi.uni_products_id','=','pr.id')
                    ->select('pr.path','pi.filename_webp')
                    ->get();

            $var=[];
            // foreach($var_img as $n=>$e_var)
            // {
            //     // dd($e_var);
            //     if($e_var->warna!=null)
            //         {
            //             // dd("else");
            //             // dd($e_var->warna);
            //             if($e_var->hex_color==""){
            //                 // dd("ini");
            //                 $code_hex="#000000 50%, #000000 50%";
            //             }else{
            //                 // dd("saya");
            //                 $colors=explode(',', $e_var->hex_color);
            //                 $list_color=[];
            //                 foreach($colors as $col)
            //                     {
            //                         array_push($list_color,$col." 50%");
            //                     }
            //                 $code_hex=$list_color[0].','.$list_color[1];

            //             }
            //             $var[$n]=["nama"=>$e_var->warna,
            //                     "code"=>$code_hex,
            //                     "image"=>$e_var->image];
            //         }
            // }
            // dd($var_img);
            $gam=db::table('uni_product_images')
                    ->where('uni_products_id',$p->id)
                    ->orderby('no','ASC')
                    ->get();




            if(isset($var_img[0])){
                $g=$var_img[0]->path.'/'.$var_img[0]->filename_webp;
            }else{
                $g='images/noimage.png';
            }
            $a_v=[];
            $data=[
                    "nama"=>$p->name,
                    "slug"=>$p->slug,
                    "gambar"=>$g,
                    "var"=>$p->name
                    ];
            array_push($list_p,$data);
            // dd($var_img);
        }


        // $berita = DB::connection('pgsql2')->table('db_news')
        //         ->where('judul','ILIKE','%'.$request->cari.'%')
        //         ->paginate(4);


        return view('search',["banner"=>$banner,"req"=>$request->cari,"produks"=>$produks,"cari"=>$request->cari,"produk"=>$list_p,"tot_produk"=>$total_produkss]);
    }

        public function katalog()
    {
        //
        // $katalog = UserDetail::find($user->id);
        // $data["info"] = $katalog;
        // $pdf = PDF::loadView('whateveryourviewname', 'Katalog-Olymplast-2020.pdf');
        // return $pdf->stream('whateveryourviewname.pdf');
        return response()->download('Katalog.pdf');
        // Redirect::away('Katalog-Olymplast.pdf');
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
