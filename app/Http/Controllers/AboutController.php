<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use DB;

class AboutController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $banner=db::table('uni_banners as a')
                    ->join('uni_banner_applications as b','a.id','=','b.uni_banners_id')
                    ->where('a.name','tentang')
                    ->where('a.status',1)
                    ->where('b.uni_gen_applications_id',2)
                    ->first();
        //dd($banner);
        $meta=db::table('uni_pages')
        ->where('uni_gen_applications_id',3)
        ->where('status',1)
        ->where('page_name',"About")
        ->orderBy('id','ASC')
        ->first();
        $about1=db::table('uni_single as a')
                    ->where('a.uni_gen_applications_id',3)
                    ->where('name','about1')
                    ->where('status',1)
                    ->first();
        $about2=db::table('uni_single as a')
                    ->where('a.uni_gen_applications_id',3)
                    ->where('name','about2')
                    ->where('status',1)
                    ->first();

        

        return view('about',["about1"=>$about1,"about2"=>$about2,"banner"=>$banner,"meta"=>$meta]);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
