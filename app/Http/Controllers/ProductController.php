<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use DB;
use Response;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\URL;


use Illuminate\Pagination\Paginator;
use Illuminate\Support\Collection;
use Illuminate\Pagination\LengthAwarePaginator;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function indexs(Request $request)
    {
            $list_warna = [];
            $_war_fil = [];
            $war_fil = [];
            $ktg_produk = [];
            $nama = "";
            $meta_title = "";
            $meta_desc = "";
            $route = str_replace('category/','',request()->path());
            $banner = db::table('uni_banners')
                    ->where('order_data',0)
                    ->where('name','product')
                    ->first();
            
            //dd(request()->path());
            $meta=db::table('uni_pages')
            ->where('uni_gen_applications_id',3)
            ->where('status',1)
            ->where('page_name',"Product")
            ->orderBy('id','ASC')
            ->first();

            if(empty($request->ktg))
            {
                $request->ktg=[];

                $ktg_produk = db::table('uni_product_olympic_view as p')
                            ->select('p.id')
                            ->orderby('id','DESC')
                            ->get();

                $nama="Semua Produk Olympic";
                $req="all";
                
            }

            if($request->new)
            {
                $ktg_produk=db::table('db_terbaru_olym')
                            ->select('id_produk')
                            ->get();
                $nama="Terbaru";
                $req="new";
            }

            if($request->q)
            {
                $request->ktg=["Meja Tulis","Meja Makan","Meja Tamu","Meja Belajar","Meja Rias","Lemari Pakaian","Lemari Anak","Lemari Hias","Lemari Sepatu","Lemari Pajangan","Ranjang","Nakas","Rak Serbaguna","Rak Dapur","Rak TV","Sofa"];
                $request->ktg=[];

                $ktg_produk = db::table('uni_product_olympic_view as p')
                                
                            ->select('p.id')
                            ->get();
                
                $nama="Semua Produk Olympic";
                $req="all";
            }
            if($request->nama)
            {

                $ktg_produk = db::table('brands_olymplast_products_view as p')
                    ->select('p.id','p.category')
                    ->where('slug_category',$request->nama)
                    ->orderby('id','DESC')
                    ->get();
                $nama_category = db::table('uni_categories as p')
                    ->select('p.name','p.meta_title','p.meta_desc')
                    ->where('slug',$request->nama)
                    ->where('uni_gen_applications_id',3)
                    ->first();
                $banner = db::table('uni_categories')
                    ->select('banner as image_desktop')
                    ->where('uni_gen_applications_id',3)
                    ->where('slug',$request->nama)
                    ->first();
                
                //dd($banner);
                $nama=$nama_category->name;
                $meta_title=$nama_category->meta_title;
                $meta_desc=$nama_category->meta_desc;
                $req="all";
            }
            
            elseif($request->ktg)
            {
                // if(count($request->ktg)==1)
                // {
                //     $banner = db::table('uni_categories')
                //             ->select('banner as image_desktop')
                //             ->where('uni_gen_applications_id',3)
                //             ->where('status','1')
                //             ->where('name',$request->ktg[0])
                //             ->first();
                // }
   
                
                    $ktg = db::table('brands_olymplast_products_view as p')
                                
                            ->select('p.id')
                            ->where('p.category',$request->ktg)
                            ->get();
                    //dd(strtoupper($k));
                    $nama=$nama.', '.$request->ktg;
                    foreach($ktg as $p){
                                 array_push($ktg_produk,$p);
                             }
                
                
                $nama=substr($nama, 1);

            }

            
            //dd($route);
            $req="nama";

            $k_k=[];
            foreach($ktg_produk as $k)
            {
                array_push($k_k,$k->id);
            }


            $data = [];
            
            foreach($k_k as $loop=>$p)
            {
                $nama_pro = db::table('uni_products')
                            ->where('id',$p)
                            ->select('name','slug')
                            ->first();

                $cari_img_p = db::table('uni_product_images as i')
                            ->join('uni_products as p','p.id','i.uni_products_id')
                            ->where('i.uni_products_id',$p)
                            ->where('i.status_brand',1)
                            ->where('i.type','PRODUK')
                            ->orderby('i.no','ASC')
                            ->select('p.path','i.filename_webp')
                            ->get();
                
                

                $data[$loop]['nama'] = $nama_pro->name;
                $data[$loop]['nama_slug'] = $nama_pro->slug;
                //$data[$loop]['status'] = $nama_pro->status;
                $data[$loop]['code'] = $p;
                if (count($cari_img_p)>0) {
                    foreach ($cari_img_p as $_loop => $img) {
                            $data[$loop]['gambar'][$_loop]['img'] = $img->path.'/'.$img->filename_webp;
                    }
                } else {
                        $data[$loop]['gambar'][0]['img'] = 'images/noimage.png';
                }
                //dd($nama_pro);
            }
            //dd($data);
           
            //SIDE MENU
                $ktg_gen=db::table('uni_categories_view')
                            ->where('uni_gen_applications_id',3)
                            ->orderby('id','ASC')
                            ->get();
            $array_ktg = [];
            foreach($ktg_gen as $n_k=>$ktg)
            {
                
                $hitung_ktg=db::table('uni_categories_view')
                            ->where('uni_categories_id',$ktg->id)
                            ->get();
                $array_ktg[$n_k]=[
                    
                    "nama"=>$ktg->name,
                    "slug"=>$ktg->slug,
                    // "active"=>(in_array($ktg->name,$request->ktg)? "checked":""),
                    "count"=>count($hitung_ktg)
                    
                ];
                
            }

            //dd($ktg);
            $count_all = db::table('uni_categories_view')
                        ->wherenotnull('uni_categories_id')
                        ->count();

            // warna
                $warna_pilih = "";
               
            if($meta_title){
                $meta_titles=$meta_title;
            }else{
                $meta_titles=$meta->meta_title;
            }

            if($meta_desc){
                $meta_descs=$meta_desc;
            }else{
                $meta_descs=$meta->meta_desc;
            }
            $side_menu = array('all'=>$count_all,"ktg"=>$array_ktg,"warna"=>$war_fil,"count"=>count($data),"nama"=>$nama,"meta_desc"=>$meta_descs,"meta_title"=>$meta_titles);
            $data = collect($data);

            $data = $this->paginate($data)->appends(request()->query());

            if($request->q)
            {
                $data->withPath(route('product',['q'=> "all"]));
            }
            elseif($request->ktg)
            {
                $data->withPath(route('product',['ktg'=> $request->ktg]));

            }
            $parent_cat = db::table('uni_categories')
                ->where('uni_gen_applications_id',3)
                ->whereNull('uni_categories_id')
                ->get();
            $child_cat = db::table('uni_categories')
                ->where('uni_gen_applications_id',3)
                ->whereNotNull('uni_categories_id')
                ->get();
            
        return view('product',["banner"=>$banner,"parent_cat"=>$parent_cat,"child_cat"=>$child_cat,'produk'=>$data,'side_menu'=>$side_menu,"req"=>$req,"meta"=>$meta,"path"=>$path]);
    }
    public function index(Request $request)
    {
        $nama_category = "";
        
       if($request->cat)
            {
                $produk = db::table('brands_olymplast_products_view')
                        ->where('category',$request->cat)
                        ->orderby('id','DESC')
                        ->paginate(12);
                $kategori = db::table('uni_categories')
                        // ->select('name')
                        ->where('uni_gen_applications_id',3)
                        ->where('slug',$request->cat)
                        ->first();
                $req = $kategori->name;
                $meta=db::table('uni_categories')
                        ->select('meta_title','meta_desc','type as seo_type', 'name','type as keyword', 'slug', 'image as seo_6')
                        ->where('uni_gen_applications_id',3)
                        ->where('status',1)
                        ->where('slug',$kategori->slug)
                        ->orderBy('id','ASC')
                        ->first();
            }else{
                $c_id = '';
                $kategori = 'all';
                $produk = db::table('brands_olymplast_products_view')
                        ->orderby('id','DESC')
                        ->paginate(12);
                $req = "Semua Produk";
                $meta=db::table('uni_pages')
                        ->where('uni_gen_applications_id',3)
                        ->where('status',1)
                        ->where('page_name',"Product")
                        ->orderBy('id','ASC')
                        ->first();
            }
        $all_kategori = db::table('uni_categories')
                ->where('uni_gen_applications_id',3)
                ->get();
        $path = request()->path();
        $series = db::table('brands_olymplast_products_view')
                    ->select('series')
                    ->whereNotNull('series')
                    ->groupby('series')
                    ->get();
        // $imgprod=db::table('brands_olymplast_product_list_image_view')
        //             ->get();
        //dd($sibling_cat);
        //dd($kategori);
        return view('product',["all_kategori"=>$all_kategori,"meta"=>$meta,"kategori"=>$kategori,"path"=>$path,"produk"=>$produk,"req"=>$req]);
    }
    public function search(Request $request)
    {

        $banner=db::table('uni_banners')
                    ->select('image_desktop as image_desktop')
                    ->where('uni_gen_applications_id',3)
                    // ->where('name',$request->nama)
                    ->where('name','pencarian')
                    ->where('status',1)
                    ->first();


        //dd($request->produk);
        $key = str_replace(" "," | ",$request->produk);

        $data= db::table('brands_olymplast_products_view')
                    ->select( db::raw("brands_olymplast_products_view.id , ts_rank(to_tsvector('indonesian', name || ' ' || keyword), to_tsquery('indonesian', '$key')) AS rank"))
                    ->whereRaw("to_tsvector('english', name || ' ' || keyword) @@ to_tsquery('english', '$key')")
                    ->orderBy('rank','desc')

                    ->get();
                    //->paginate(4);
        
        $id_data=[];
        foreach($data as $we)
            {
                array_push($id_data,$we->id);
            }

        //dd($id_data);
        $produk=db::table('brands_olymplast_products_view')
            ->select('id','name','judul_brand','slug','images','category_name')
            ->whereIn('id', $id_data)
            ->orderBy('id','DESC')
            ->paginate(12);
        //dd($produk);
        $imgprod=db::table('brands_olympic_product_variants_view')
                ->get();

        $databerita=db::table('brands_olympic_news_list_view')
                ->where('title','ILIKE','%'.$request->produk.'%')
                ->where('content','ILIKE','%'.$request->produk.'%')
                ->get();
        $berita=db::table('brands_olympic_news_list_view')
                ->where('title','ILIKE','%'.$request->produk.'%')
                ->where('content','ILIKE','%'.$request->produk.'%')
                ->paginate(4);
        $parent_cat = db::table('uni_categories')
                ->where('uni_gen_applications_id',3)
                ->whereNull('uni_categories_id')
                ->get();
        $child_cat = db::table('uni_categories')
                ->where('uni_gen_applications_id',3)
                ->whereNotNull('uni_categories_id')
                ->get();
        return view('search',["parent_cat"=>$parent_cat,"child_cat"=>$child_cat,"banner"=>$banner,"req"=>$request->produk,"cari"=>$request->search,"berita"=>$berita,"produk"=>$produk,"data"=>$data,"imgprod"=>$imgprod,"databerita"=>$databerita]);
    }

    public function api_p($id) {
        $produk=db::table('uni_product_images as pi')
            // ->select('pi.filename_webp','name','judul_brand','slug','images')
            ->select(db::raw('CONCAT(p.path, \'/\', pi.filename_webp) as images'))
            ->leftjoin('uni_products as p','p.id','=','pi.uni_products_id')
            ->where('pi.uni_products_id', $id)
            ->where('pi.type','PRODUK')
            ->orderBy('pi.no','DESC')
            ->get();
        //dd($produk);
        return response()->json($produk);
    }
    public function detail(Request $request) {
            $produk = db::table('brands_olymplast_products_view')
                    ->where('slug',$request->nama)
                    ->first();
            //dd($produk);
            $kelebihan  = db::table('uni_product_superiorities')
                    ->where('uni_products_id',$produk->id)
                    ->get();
            $manuals  = db::table('uni_product_raft_guides')
                    ->where('uni_products_id',$produk->id)
                    ->get();
            $variant_name  = db::table('uni_products')
                    ->where('id',$produk->id)
                    ->wherenotnull('variant_1_name')
                    ->first();
            
            $variant = db::table('uni_product_variants as pv')
                        // ->distinct('pv.variant')
                        ->select('pv.variant','pv.id')
                        // ->wherenull('pv.status')
                        // ->orwhere('pv.status', 0)
                        // ->join('uni_product_images as pi','pi.uni_products_variants_id','pv.id')
                        // ->join('uni_product_images as pi','pi.uni_products_variants_id','pv.id')
                        ->where('pv.uni_products_id',$produk->id)
                        
                        //->orderBy('show_on', 'asc')
                        ->wherenotnull('pv.variant')
                        ->where(function($query){
                            return $query
                            ->whereNull('pv.status')
                            ->orWhere('pv.status', 1);})
                        
                        ->groupBy('pv.variant','pv.id')
                        ->get();
            //dd($variant); 
            $variant_2 = db::table('uni_product_variants as pv')
            // ->distinct('pv.variant')
            ->select('pv.variant_2')
            // ->join('uni_product_images as pi','pi.uni_products_variants_id','pv.id')
            ->where('pv.uni_products_id',$produk->id)
            ->where('pv.status','!=' , 0)
            ->wherenotnull('pv.variant_2')
            //->orderBy('show_on', 'asc')
            ->groupBy('pv.variant_2')
            ->get();
            //dd($variant); 
            $varian = db::table('uni_product_variants as pv')
                        ->distinct('pv.variant','pv.variant_2')
                        // ->select('pv.variant')
                        // ->join('uni_product_images as pi','pi.uni_products_variants_id','pv.id')
                        ->where('pv.uni_products_id',$produk->id)
                        ->where(function($query){
                            return $query
                            ->whereNull('pv.status')
                            ->orWhere('pv.status', 1);})
                        // ->orderByRaw('CAST(variant as INTEGER) ASC')
                        
                        // ->orderBy('pv.variant', 'asc')
                        // ->groupBy('pv.variant')
                        ->get();
            //dd($varian);               
            $variant_3 = db::table('uni_product_variants as pv')
                        ->select('color')
                        ->where('pv.uni_products_id',$produk->id)
                        ->groupBy('color')
                        ->orderByRaw('CHAR_LENGTH("color")')
                        ->get();
                        //dd($ukuran);
            $images = db::table('uni_product_variants as pv')
                        ->distinct('pv.id')
                        ->join('uni_product_images as pi','pi.uni_products_variants_id','pv.id')
                        ->where('pv.uni_products_id',$produk->id)
                        //->orderBy('show_on', 'asc')
                        ->get();
            //dd($images); 
            $material = db::table('uni_product_variant_materials as vm')
                        ->select('mm.name')
                        ->distinct('mm.id')
                        ->join('uni_gen_materials as mm','vm.uni_gen_materials_id','mm.id')
                        ->join('uni_product_variants as pv','vm.uni_product_variants_id','pv.id')
                        ->where('pv.uni_products_id',$produk->id)
                        //->orderBy('show_on', 'asc')
                        ->get();
            $fitur=db::table('uni_product_superiorities as ps')
                        ->leftjoin('uni_features as f','ps.name','=','f.name')
                        ->where('ps.uni_products_id',$produk->id)
                        ->wherenotnull('ps.name')
                        ->wherenotnull('ps.image')
                        ->orderBy('f.id', 'asc')
                        ->get();
            //dd($fitur);
            $dim = db::table('uni_product_variants')
                    ->where('uni_products_id',$produk->id)
                    ->wherenotnull('dimensions_length')
                    ->get();
            $superior = db::table('uni_product_superiorities')
                    ->where('uni_products_id',$produk->id)
                    ->get();
            $more_on = db::table('uni_product_applications')
                        ->where('uni_products_id',$produk->id)
                        ->where('uni_gen_applications_id',1)
                        ->first();
            $gambar = db::table('uni_product_images as i')
                    ->join('uni_products as p','p.id','i.uni_products_id')
                    ->where('i.uni_products_id',$produk->id)
                    ->where('i.status_brand',1)
                    ->where('i.type','PRODUK')
                    ->orderby('i.no','ASC')
                    ->select('p.path','i.filename_webp','i.no','i.uni_products_variants_id')
                    ->get();
            $p_detail = db::table('uni_product_images as i')
                    ->join('uni_products as p','p.id','i.uni_products_id')
                    ->where('i.uni_products_id',$produk->id)
                    ->where('i.status_brand',1)
                    // ->where('i.type','PRODUK-DETAIL')
                    ->orderby('i.type','ASC')
                    ->orderby('i.no','ASC')
                    ->select('p.path','i.filename_webp','i.no','i.uni_products_variants_id','i.type')
                    ->get();
            $foto_utama = db::table('uni_product_image_view')
                    ->where('uni_products_id',$produk->id)
                    ->first();
            $foto_kedua = db::table('uni_product_image2_view')
                    ->select('image_alt')
                    ->where('uni_products_id',$produk->id)
                    ->first();
            //dd($p_detail);
            // $material = db::table('db_produk_varian_material')
            //         ->where('db_produk_varian_material.id_produk',$produk->id_produk)
            //         ->select('db_kategori_material.nama')
            //         ->join('db_kategori_material','db_kategori_material.id_material','db_produk_varian_material.id_material')
            //         ->distinct()
            //         ->get();
            //dd($produk);
            $list_g_s = [];
            //dd($gambar);
            foreach($gambar as $g)
            {
                $i_pict=[
                    "img"=>$g->path.'/'.$g->filename_webp,
                    "no"=>$g->uni_products_variants_id
                ];

                array_push($list_g_s,$i_pict);
            }

            // $tags = explode('>', $produk->desc);
            // $list_tag = [];
            //     if($tags[0] != "")
            //     {
            //         foreach($tags as $tag)
            //         {
            //             if (stripos(json_encode($list_tag),$tag) == false) {
            //                 array_push($list_tag,$tag.">");
            //             }
            //         }
            //     }

            if(isset($more_on->uni_gen_applications_id)){
                $detail = [

                    "id" => $produk->id,
                    "nama" => $produk->name,
                    "path" => $produk->path,
                    "m_title" => $produk->meta_title_brand,
                    "m_desc" => $produk->meta_desc_brand,
                    "m_keyword" => $produk->keyword_brand,
                    "warna" => [],
                    "desc"=>$produk->name,
                //  "desc" => $list_tag,
                //  "desc_text" =>$produk->desc_text,
                    "sku"=>$produk->sku_parent,
                    "brand"=>$produk->brand,
                    "dimensi" => [],
                    "gambar_non_webp"=>$foto_kedua->image_alt,
                    "gambar" => $list_g_s,
                    "category_name" => $produk->category_name,
                    "category_slug" => $produk->slug_category,
                // "material" => $material,
                    "slug"=>$produk->slug,
                    "show"=>$more_on->uni_gen_applications_id
                    
                ];
            } else {
                $detail = [

                    "id" => $produk->id,
                    "nama" => $produk->name,
                    "path" => $produk->path,
                    "m_title" => $produk->meta_title_brand,
                    "m_desc" => $produk->meta_desc_brand,
                    "m_keyword" => $produk->keyword_brand,
                    "warna" => [],
                    "desc"=>$produk->name,
                //  "desc" => $list_tag,
                //  "desc_text" =>$produk->desc_text,
                    "sku"=>$produk->sku_parent,
                    "brand"=>$produk->brand,
                    "gambar_non_webp"=>$foto_kedua->image_alt,
                    "dimensi" => [],
                    "gambar" => $list_g_s,
                    "category_name" => $produk->category_name,
                    "category_slug" => $produk->slug_category,
                // "material" => $material,
                    "slug"=>$produk->slug,
                    "show"=>null
                    
                ];
            }
            
            // dd($detail);
            $detail['warna'] = [];
            $video=db::table('uni_product_videos')
                        ->where('uni_products_id',$detail['id'])
                        ->where('status',1)
                        ->get();
        

        

        foreach($varian as $urut => $var)
        {

            if(isset($var->warna))
            {
                if(isset($var->hex_color[0]))
                {

                $tags = explode(',', $var->hex_color);
                $list_tag = [];

                foreach($tags as $tag)
                {
                        array_push($list_tag,$tag." 50%");

                }

                $code_hex = $list_tag[0].','.$list_tag[1];
                    }else{                            $code_hex="#000000 50%, #000000 50%";}
                    $detail['warna'][$urut] = [
                        "nama" => $var->warna,
                        "var" => $var->varian_image,
                        "code" => $code_hex];
            }
            if(isset($var->varian))
            {
                $detail['varian'][$urut] = [
                            "nama" => $var->varian,
                            "var" => $var->varian_image,
                            "image"=>$var->varian_image,
                            "p" => $var->dimensions_length,
                            "l" => $var->dimensions_width,
                            "t" => $var->dimensions_height
                            ];

                $detail['dimensi'][$urut] = [
                    "nama" => $var->varian,
                    "p" => $var->dimensions_length,
                    "l" => $var->dimensions_width,
                    "t" => $var->dimensions_height
                ];
            }else{

                $detail['dimensi'][0]=[
                    "nama" => "",
                    "p" => $var->dimensions_length,
                    "l" => $var->dimensions_width,
                    "t" => $var->dimensions_height
                ];
            }

        }

        $id_ktg = $produk->slug_category;
        $terkait = db::table('brands_olymplast_products_view')
                    ->where('slug_category',$id_ktg)
                    ->inRandomOrder()
                    ->limit(8)
                    ->where('name','!=',$request->nama)
                    ->get();

        if(count($terkait)<4)
        {
            
            $sekategori=db::table('brands_olymplast_products_view')
                ->where('slug_category',$id_ktg)
                ->where('name','!=',$request->nama)
                ->inRandomOrder()
                ->limit(8)
                ->get();
            $sekategori2=db::table('brands_olymplast_products_view')
                    ->wherenotnull('category')
                    ->where('name','!=',$request->nama)
                    // ->union($sekategori)
                    ->inRandomOrder()
                    ->limit(8)
                    ->get();
            $terkait=$sekategori->union($sekategori2);
        //dd($terkait);
        }

        $data = [];
        $list_warna = [];
        $war_fil = [];

        foreach($terkait as $n_p=>$p)
        {
            $cari_img_p=

                        db::table('uni_product_images as i')
                            ->join('uni_products as p','p.id','i.uni_products_id')
                            ->where('i.uni_products_id',$p->id)
                            ->where('i.status_brand',1)
                            ->where('i.type','PRODUK')
                            ->orderby('i.no','ASC')
                            ->select('p.path','i.filename_webp')
                            ->first();

            $warna=db::table('uni_product_variants as var')
                    ->whereNotNull('var.color')
                    ->where('var.uni_products_id',$p->id)
                    ->select('var.id','var.color','var.hex_color','img.filename_webp','img.no')
                    ->leftjoin('uni_product_images as img','img.uni_products_variants_id','=','var.id')
                    ->get();
            // dd($warna);
            $vari=[];
            if(isset($warna[0]))
            {
                foreach($warna as $n_v=>$w)
                {

                    $wa=["nama"=>$w->color,
                            "code"=>$w->hex_color];
                    
                    $w_s=db::table('uni_product_variant_colors as vc')
                            ->where('uni_product_variants_id',$w->id)
                            ->join('uni_gen_colors as gc','gc.id','=','vc.uni_gen_colors_id')
                        //    ->join('db_code_warna','db_code_warna.nama','=','db_kategori_warna.nama')
                            ->get();

                    if(isset($w_s[0])){

                        foreach($w_s as $w_s_e)
                        {
                            $war_s_fil=[
                                "nama"=>$w_s_e->nama,
                                "code"=>$w_s_e->code
                            ];
                            array_push($war_fil,$war_s_fil);
                        }
                    }
                    array_push($list_warna,$wa);
                    $cari_img_var_warna=db::table('uni_product_images')
                                    ->where('uni_products_id',$p->id)
                                    ->where('uni_products_variants_id',$w->id)
                                    ->get();
                    if(isset($cari_img_var_warna[0]))
                    {
                        $img_var_warna=$cari_img_var_warna[0]->filename_webp;
                    }else{
                        $img_var_warna="images/noimage.png";
                    }
                    $vari[$n_v]=[
                        "warna"=>$w->color,
                        "code"=>$w->hex_color,
                        "image"=>$img_var_warna
                    ];
                }
            }
            if(isset($p->hex_color[0]))
            {

            $tags=explode(',', $w->hex_color);
            $list_tag=[];
            // dd($tags);
            foreach($tags as $tag)
            {
                    array_push($list_tag,$tag." 50%");

            }
            if(isset($list_tag[0]))
                {}else{dd("gak ada sayang list nya");}
            $code_hex=$list_tag[0].','.$list_tag[1];
            }else{
                $code_hex="#000000 50%, #000000 50%";
            }
            $data[$n_p]=[
                "nama"=>$p->judul_brand,
                "nama_slug"=>$p->slug,
                "code"=>$p->id,
                "hex"=>$code_hex,
                "gambar"=>(($cari_img_p) ? ($cari_img_p->path.'/'.$cari_img_p->filename_webp) : 'images/noimage.png'),
                "varian"=>$variant_name,
                "category"=>$p->category,
            ];
        }


         //dd($terkait);

        return view('product-detail',['p_detail'=>$p_detail,'variant_name'=>$variant_name,'variant_name'=>$variant_name,'variant_name'=>$variant_name,'material'=>$material,'fitur'=>$fitur,'kelebihan'=>$kelebihan,'varian'=>$varian,'variant'=>$variant,'variant_2'=>$variant_2,"variant_3"=>$variant_3,'detail'=>$detail,'terkait'=>$data,"video"=>$video,'foto_utama'=>$foto_utama,'manuals'=>$manuals,'more_on'=>$more_on]);
    }

    public function paginate($items, $perPage = 30, $page = null, $options = [])
    {
        $page = $page ?: (Paginator::resolveCurrentPage() ?: 1);
        $items = $items instanceof Collection ? $items : Collection::make($items);
        return new LengthAwarePaginator($items->forPage($page, $perPage), $items->count(), $perPage, $page, $options);
    }
    public function searchs(Request $request)
    {

        $banner=db::table('uni_banners')
        ->where('name','pencarian')
        ->where('status',1)
        ->first();
        
        // $produks=db::table('uni_product_olympic_view')
        //         ->where('uni_product_olympic_view.name','ILIKE','%'.$request->cari.'%')
        //         ->groupBy('uni_product_olympic_view.id')
        //         ->select('uni_product_olympic_view.id')
        //         ->paginate(8);
        if(isset($request->cari)){
            DB::table('uni_search_history')->insert([
                'uni_gen_applications_id' => 7,
                'text' => $request->cari,
                'ip' => request()->ip()
            ]);
        }


        $produks = db::table('uni_product_olympic_view as p')
                ->select('p.id')
                ->where('p.name','ILIKE','%'.$request->cari.'%')
                ->orderby('id','DESC')
                ->paginate(8);
        $total_produks = db::table('uni_product_olympic_view as p')
                ->select('p.id')
                ->where('p.name','ILIKE','%'.$request->cari.'%')
                ->get();
        $total_produkss = $total_produks->count(); 
         //dd($total_produk);
        $list=[];
        foreach($produks as $pr)
        {
            $detail=db::table('uni_product_olympic_view')
                    ->where('id',$pr->id)
                    ->get();

            array_push($list,$detail[0]);
        }
        $list_p=[];
        //dd($list);
        foreach($list as $p)
        {
            $var_img=db::table('uni_products as pr')
                    ->where('pr.id',$p->id)
                    ->leftjoin('uni_product_images as pi','pi.uni_products_id','=','pr.id')
                    ->select('pr.path','pi.filename_webp')
                    ->get();

            $var=[];
            // foreach($var_img as $n=>$e_var)
            // {
            //     // dd($e_var);
            //     if($e_var->warna!=null)
            //         {
            //             // dd("else");
            //             // dd($e_var->warna);
            //             if($e_var->hex_color==""){
            //                 // dd("ini");
            //                 $code_hex="#000000 50%, #000000 50%";
            //             }else{
            //                 // dd("saya");
            //                 $colors=explode(',', $e_var->hex_color);
            //                 $list_color=[];
            //                 foreach($colors as $col)
            //                     {
            //                         array_push($list_color,$col." 50%");
            //                     }
            //                 $code_hex=$list_color[0].','.$list_color[1];

            //             }
            //             $var[$n]=["nama"=>$e_var->warna,
            //                     "code"=>$code_hex,
            //                     "image"=>$e_var->image];
            //         }
            // }
            // dd($var_img);
            $gam=db::table('uni_product_images')
                    ->where('uni_products_id',$p->id)
                    ->orderby('no','ASC')
                    ->get();




            if(isset($var_img[0])){
                $g=$var_img[0]->path.'/'.$var_img[0]->filename_webp;
            }else{
                $g='images/noimage.png';
            }
            $a_v=[];
            $data=[
                    "nama"=>$p->name,
                    "slug"=>$p->slug,
                    "gambar"=>$g,
                    "var"=>$p->name
                    ];
            array_push($list_p,$data);
            // dd($var_img);
        }


        // $berita = DB::connection('pgsql2')->table('db_news')
        //         ->where('judul','ILIKE','%'.$request->cari.'%')
        //         ->paginate(4);

        //dd($produk);
        return view('search',["banner"=>$banner,"req"=>$request->cari,"produks"=>$produks,"cari"=>$request->cari,"produk"=>$list_p,"tot_produk"=>$total_produkss]);
    }

        public function katalog()
    {
        //
        // $katalog = UserDetail::find($user->id);
        // $data["info"] = $katalog;
        // $pdf = PDF::loadView('whateveryourviewname', 'Katalog-Olymplast-2020.pdf');
        // return $pdf->stream('whateveryourviewname.pdf');
        return response()->download('Katalog.pdf');
        // Redirect::away('Katalog-Olymplast.pdf');
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
