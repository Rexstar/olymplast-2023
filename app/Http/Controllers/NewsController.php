<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use DB;
use Response;
use Illuminate\Support\Str;
use App\Http\Controllers\Controller;
use App\Models\Tag;

use Illuminate\Pagination\Paginator;
use Illuminate\Support\Collection;
use Illuminate\Pagination\LengthAwarePaginator;


class NewsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    

    
    public function blog(Request $request)
    {
        if($request->cat)
        {
        $news=DB::table('brands_olymplast_news_list_view')
            ->distinct('id')
            ->where('cat_slug',$request->cat)
            ->orderby('id','DESC')
            ->orderby('date','DESC')
            ->paginate(6);
        $meta=db::table('uni_news_categories')
            ->select('*', 'name_id as page_name')
            ->where('uni_gen_applications_id',3)
            ->where('statuss',1)
            ->where('slug',$request->cat)
            ->orderBy('id','ASC')
            ->first();
        $cat='cat/';
        $mode = 'cat';
        //dd($meta);
        }
        elseif($request->tag)
        {
            $news=DB::table('brands_olymplast_news_list_view as nws')
            ->distinct('nws.id')
            ->join('brands_olymplast_news_tags_view as tag','nws.id','=','tag.id')
            ->where('tag.name',$request->tag)
            ->orderby('nws.id','DESC')
            ->orderby('nws.date','DESC')
            ->paginate(6);
        $meta=db::table('brands_olymplast_news_tags_view as tag')
            // ->select('list.slug')
            ->select([ DB::raw("'{$request->tag}  | Olymplast Plastic Furniture' as meta_title, list.description_meta as meta_desc, list.keyword, tag.name as slug, tag.name as page_name")])
            // ->selectRaw('"?" as username' [$request->tag])
            ->where('tag.name',$request->tag)
            ->join('brands_olymplast_news_list_view as list','list.id','=','tag.id')
            ->orderBy('tag.id','ASC')
            ->first();
        $cat='tag/';
        $mode = 'tag';

        
        }else{
        $news=DB::table('brands_olymplast_news_list_view')
        ->distinct('id')
        ->orderby('id','DESC')
            ->orderby('date','DESC')
            ->paginate(6);
        $meta=db::table('uni_pages')
            ->where('uni_gen_applications_id',3)
            ->where('status',1)
            ->where('page_name','Blog')
            ->orderBy('id','ASC')
            ->first();
        $cat='';
        $mode = 'all';

        }
        $categories=DB::table('uni_news_categories as c')
            ->select('nw.name_id','c.slug','c.image')
            ->distinct('nw.cat_id')
            ->where('c.statuss',1)
            ->join('brands_olymplast_news_list_view   as nw','nw.cat_id','=','c.id')
            // ->whereNotNull('uni_news_categories_id')
            ->get();
        $ncategories=DB::table('uni_news_categories as c')
            ->where('c.statuss',1)
            ->join('uni_news_categories_details as cd','c.id','=','cd.uni_news_categories_id')
            ->get();
        $banner_mobile = db::table('uni_banners as b')
            ->where('b.uni_gen_applications_id',3)
            ->where('b.name','blog')
            ->where('b.status',1)
            ->where(function($q) {
                $q->where('b.duration_start','<=',now())
                ->orWhereNull('b.duration_start');
            })
            ->where(function($q) {
                $q->where('b.duration_end','>=',now())
                ->orWhereNull('b.duration_end');
            })
            ->whereNotNull('image_desktop')
            ->orderBy('order_data','ASC')
            ->get();

        $banner_pc = db::table('uni_banners as b')
            ->where('b.uni_gen_applications_id',3)
            ->where('b.name','blog')
            ->where('b.status',1)
            ->where(function($q) {
                $q->where('b.duration_start','<=',now())
                ->orWhereNull('b.duration_start');
            })
            ->where(function($q) {
                $q->where('b.duration_end','>=',now())
                ->orWhereNull('b.duration_end');
            })
            ->whereNotNull('image_desktop')
            ->orderBy('order_data','ASC')
            ->get();
        $tags = DB::table('brands_olymplast_news_tags_view')
            ->select('name', DB::raw('count(*) as total'))
            ->groupBy('name')
            ->orderBy('total','DESC')
            ->get();
        $sliders = DB::table('brands_olymplast_news_list_view')
            ->distinct('id')
            ->where('slider_article',1)
            ->orderBy('id','DESC')
            ->orderBy('date','DESC')
            ->limit(5)
            ->get();
        $ncategories=DB::table('uni_news_categories as c')
            ->join('uni_news_categories_details as cd','c.id','=','cd.uni_news_categories_id')
            ->get();
        //dd($sliders);
        return view('news',["ncategories"=>$ncategories,"sliders"=>$sliders,"mode"=>$mode,"tags"=>$tags,"cat"=>$cat,"meta"=>$meta,"banner_pc"=>$banner_pc,"banner_mobile"=>$banner_mobile,"news"=>$news,"categories"=>$categories,"ncategories"=>$ncategories]);
    }

    public function blog_detail(Request $request)
    {
        $detail=db::table('brands_olymplast_news_list_view')
        ->where('slug',$request->blog)
        ->first(); 
        
        $blogs = db::table('brands_olymplast_news_list_view')
            ->where('name_id', $detail->name_id)
            ->where('slug','!=',$request->blog)
            ->select('slug','title','image','date','name_id','author')
            ->distinct('id')
            ->orderby('id','DESC')
            ->orderby('date','DESC')
            ->limit(8)
            ->get();
        if(count($blogs)<4){
            $blogs = db::table('brands_olymplast_news_list_view')
            ->where('slug','!=',$request->blog)
            ->select('slug','title','image','date','name_id','author')
            ->distinct('id')
            ->orderby('id','DESC')
            ->orderby('date','DESC')
            ->limit(8)
            ->get();

        }
        $tags = db::table('brands_olymplast_news_tags_view')
        ->where('id', $detail->id)
        ->get();
        $products = db::table('uni_news_products as np')
        ->where('np.uni_news_id', $detail->id)
        ->join('brands_olymplast_products_view as pv','pv.id','=','np.uni_products_id')
        ->get();
       //dd($products);
        return view('news-detail',["products"=>$products,"detail"=>$detail,"blogs"=>$blogs,"tags"=>$tags]);
    }
        

    public function paginate($items, $perPage = 3, $page = null, $options = [])
    {
        $page = $page ?: (Paginator::resolveCurrentPage() ?: 1);
        $items = $items instanceof Collection ? $items : Collection::make($items);
        return new LengthAwarePaginator($items->forPage($page, $perPage), $items->count(), $perPage, $page, $options);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
