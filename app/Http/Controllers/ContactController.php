<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\Mail;
use App\Mail\SendMail;
use DB;
use App\Models\Contact;


class ContactController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        // $banner=db::table('uni_banners_categories_view')
        // ->where('name','contact')
        // ->where('uni_gen_applications_id',3)
        // ->where('status',1)
        // ->first();
        // $toko=db::connection('pgsql2')->table('db_toko')
        // ->get();
        $meta=db::table('uni_pages')
            ->where('uni_gen_applications_id',3)
            ->where('status',1)
            ->where('page_name',"Kontak")
            ->orderBy('id','ASC')
            ->first();
        // dd($toko);
        return view('contact',['meta'=>$meta]);
    }
    public function email(request $request)
    {
        // dd($request->all());
        $this->validate($request, [
            'nama'     =>  'required',
            'email'  =>  'required|email',
            'text' => 'required'
           ]);
        
        //   $dats = array(
        //       'email'     =>   $request->email,
        //       'nama'      =>   $request->nama,
        //       'message'   =>   $request->text
        //   );

          $data = [
            'nama' => $request->nama,
            'email' => $request->email,
            'message' => $request->text
        ];

        
        //   $kirim = Mail::to("contact@olymplast.co.id")->send(new SendMail($data));
          Mail::to('contact@olymplast.co.id')->send(new SendMail($data));
    
        // if($kirim){
        //     echo "Email telah dikirim";
        // }
        return redirect()->back()->with(['message' => 'Pesan terkirim. Terima kasih telah menghubungi Olympic Furniture']);
     
    }
    public function mitra()
    {
        return view('mitra');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'email' => 'required|email',
            'phone' => 'required|min:8|numeric',
            'subject' => 'required',
            'message' => 'required'
        ]);
        Contact::create($request->all());
        return redirect()->back()
                         ->with(['success' => 'Terima kasih, Pesan anda telah terkirim']);
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
