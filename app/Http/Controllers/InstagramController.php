<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
use Intervention\Image\Facades\Image;

class InstagramController extends Controller
{
    public function downloadAndSaveImages()
    {
        // Ganti dengan data akun Instagram Anda
        $userId = '2316990060';
        $accessToken = 'IGQWRONjlrVml2cDlYenc3bmFOajVSUkY2aDc5bERTaU5zWUVOZA2FfNHVyS2ZAGZAHh1cTBVZAzd1X0hrZAllQMENuX1NSVTNEYXJyamhkQ1NmN1FKS2k2SGt1U190c3hQdER3RjJYc0pjOVl2cWdmYXdfUXdwa09lS2MZD';
        

        // Ambil data dari API Instagram
        $response = Http::get("https://graph.instagram.com/me/media?fields=caption,id,media_type,media_url,permalink,thumbnail_url,timestamp,username&access_token={$accessToken}");
       
        $images = $response->json()['data'];
        //dd($images);
        // Simpan gambar di direktori tujuan

        foreach ($images as $image) {
            if ($image['media_type'] !== 'VIDEO') {
                $imageUrl = $image['media_url'];
                $nameUrl = str_replace('/', '+', $image['permalink']);
                $nameUrl = str_replace(':', ';', $nameUrl);
                //dd($nameUrl);
                //$this->saveImageFromUrl($imageUrl, "imageIG/{$nameUrl}.jpg");

                // Simpan gambar dalam format JPEG dan WebP
                $this->saveImageFromUrl($imageUrl, "imageIG/{$nameUrl}_-_small.jpg", 200);
                $this->saveImageFromUrl($imageUrl, "imageIG/{$nameUrl}_-_medium.jpg", 400);
                $this->saveImageFromUrl($imageUrl, "imageIG/{$nameUrl}_-_large.jpg", 600);

                // Simpan gambar dalam format WebP
                $this->saveImageFromUrl($imageUrl, "imageIG/{$nameUrl}_-_small.webp", 200, 'webp');
                $this->saveImageFromUrl($imageUrl, "imageIG/{$nameUrl}_-_medium.webp", 400, 'webp');
                $this->saveImageFromUrl($imageUrl, "imageIG/{$nameUrl}_-_large.webp", 600, 'webp');
            }
        }

        $statusMessage = 'Images downloaded and saved successfully!';
        return view('instagram', compact('statusMessage'));
    }

    private function saveImageFromUrl($imageUrl, $destinationPath, $width, $format = 'jpg')
    {
        $imageContent = file_get_contents($imageUrl);
         // Buat instance dari Intervention Image
        $image = Image::make($imageContent);

        // Resize gambar
        $image->resize($width, null, function ($constraint) {
            $constraint->aspectRatio();
        });

        // Simpan gambar dengan format yang ditentukan (jpg atau webp)
        $image->save(public_path($destinationPath), 80, $format);
            //dd($destinationPath);
            // file_put_contents(public_path($destinationPath), $imageContent);
        }
    // private function saveImageFromUrl($imageUrl, $destinationPath)
    // {
    //     $imageContent = file_get_contents($imageUrl);

    //     $fileName = pathinfo(parse_url($imageUrl, PHP_URL_PATH), PATHINFO_FILENAME);
        
    //     $fullPath = public_path("{$destinationPath}/{$fileName}.jpg");

    //     file_put_contents($fullPath, $imageContent);
    // }
}
