<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [\App\Http\Controllers\IndexController::class, 'index'])->name('index');
Route::get('product', [\App\Http\Controllers\ProductController::class, 'index'])->name('product');
Route::get('product/{nama}', [\App\Http\Controllers\ProductController::class, 'detail'])->name('product_detail');
Route::get('product-detail/{nama}', [\App\Http\Controllers\ProductController::class, 'detail'])->name('product_detail');
Route::get('category/{cat}', [\App\Http\Controllers\ProductController::class, 'index'])->name('category');
Route::get('about', [\App\Http\Controllers\AboutController::class, 'index'])->name('about');
Route::get('blog', [\App\Http\Controllers\NewsController::class, 'blog'])->name('blog');
Route::get('blog/{blog}', [\App\Http\Controllers\NewsController::class, 'blog_detail'])->name('blog_detail');
Route::get('news-detail/{blog}', function($blog){
    return redirect(url('blog/'.$blog.''));
});
Route::get('inspiration-detail/{blog}', function($blog){
    return redirect(url('blog/'.$blog.''));
});
Route::get('news', function(){
    return redirect(url('blog'));
});
Route::get('blog/cat/{cat}', [\App\Http\Controllers\NewsController::class, 'blog'])->name('categories');
Route::get('blog/tag/{tag}', [\App\Http\Controllers\NewsController::class, 'blog'])->name('tags');
Route::get('search', [\App\Http\Controllers\ProductController::class, 'search'])->name('produk');
Route::get('kol', [\App\Http\Controllers\IndexController::class, 'kol'])->name('kol');
//log-viewers
Route::get('log-viewers', [\Rap2hpoutre\LaravelLogViewer\LogViewerController::class, 'index']);

Route::get('contact', [\App\Http\Controllers\ContactController::class, 'index'])->name('contact');
Route::post('contact', [\App\Http\Controllers\ContactController::class, 'store'])->name('contact.store');

Route::get('/mitra', function () {
    return Redirect::to('https://mitraolympic.co.id/');
});

Route::get('/sustainability/pilih-produk-sustainable-yang-lebih-ramah-lingkungan-2', function () {
    return Redirect::to('/blog/pilih-produk-sustainable-yang-lebih-ramah-lingkungan');
});
Route::get('/sustainability/ciptakan-lingkungan-sehat-dengan-produk-sustainable-2/', function () {
    return Redirect::to('/blog/ciptakan-lingkungan-sehat-dengan-produk-sustainable');
});
Route::get('/sustainability/langkah-kecil-dapat-mengubah-masa-depan-lebih-baik/', function () {
    return Redirect::to('/blog/sustainable-product-langkah-kecil-dapat-mengubah-masa-depan');
});
Route::get('/betterlife', function () {
    return Redirect::to('https://olymplast.co.id/8etterlife');
});
Route::get("sitemap.xml" , function () {
    return \Illuminate\Support\Facades\Redirect::to('sitemap.xml');
});
Route::get('katalog', function () {
    return Redirect::to('https://page.olymplast.co.id/katalog/');
})->name('katalog');

Route::get('downloadIG', [\App\Http\Controllers\InstagramController::class, 'downloadAndSaveImages'])->name('instagram');
