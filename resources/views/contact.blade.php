@extends('template')

@section('m_title')
{{$meta->meta_title}}
@stop

@section('m_desc')
{{$meta->meta_desc}}
@stop

@section('m_type')
website
@stop

@section('m_author')
Olympic
@stop

@section('m_keywords')
{{$meta->keyword}}
@stop

@section('m_image')
https://asset.olymplast.co.id/BRAND/OLYMPIC/ASSET/og-furniture.png
@stop

@section('m_canonical')
https://olymplast.co.id/contact
@stop

@section('m_robots')
all
@stop


@section('content')
<div class=" section pt-5 pb-3 mt-5">
    <div class="container-xxl  px-4 bg-ol-grey rounded">
        <div class="row ">
            <div class="col-lg-6 p-lg-5 p-2">
                <div class="rounded overflow-hidden h-100">
                    <iframe class="ol-contact-map__iframe w-100 h-100" src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d3959.058605911803!2d112.4623557!3d-7.1192072!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2e77fa27c9dc5e41%3A0x3f07169f7937eed1!2sPT.%20Cahaya%20Bintang%20Olympic!5e0!3m2!1sid!2sid!4v1610097432911!5m2!1sid!2sid" frameborder="0" style="border:0;" allowfullscreen=""></iframe>
                </div>
            </div>
            <div class="col-lg-6 position-relative">
            <div class="position-absolute top-0 end-0 p-5 mt-3 me-2 cat-border">
            
            </div>
                <div class=" p-lg-5 p-3 pb-4 ">
                    <!-- <div class="h2 text-uppercase fw-bold cat-title col-lg-7 offset-lg-5 col-12 ps-5 text-end pt-3">Contact Us</div>
                    <div class="col-lg-8 offset-lg-4 col-12 cat-text"><p class="lh-sm text-end">Kirim pertanyaan anda ke Contact Center kami</p></div>
                    <div class="">
                        <form action="assets/php/contact.php" method="post"class="m-auto" style="max-width:600px">
                            <div class="form-group mb-3 row"><label for="your-name2" class="col-md-3 col-form-label">Your Name</label>
                                <div class="col-md-9"><input type="text" class="form-control form-control-lg" id="your-name2" name="your-name" required></div>
                            </div>
                            <div class="form-group mb-3 row"><label for="your-email3" class="col-md-3 col-form-label">Your Email</label>
                                <div class="col-md-9"><input type="email" class="form-control form-control-lg" id="your-email3" name="your-email" required><small class="form-text text-muted"> Please enter a valid email address</small></div>
                            </div>
                            <div class="form-group mb-3 row"><label for="your-message6" class="col-md-3 col-form-label">Your Message</label>
                                <div class="col-md-9"><textarea class="form-control form-control-lg" id="your-message6" name="your-message" required></textarea></div>
                            </div>
                            <div class="form-group mb-3 row"><label for="send-a-message8" class="col-md-3 col-form-label"></label>
                                <div class="col-md-9"><button class="btn btn-primary btn-lg" type="submit">Submit</button></div>
                            </div>
                        </form>
                    </div> -->
                    <div class="h2 text-uppercase fw-bold cat-title col-lg-7 offset-lg-5 col-12 ps-5 text-end pt-3  text-ol-dsblue">Contact Us</div>
                    <div class="col-lg-8 offset-lg-4 col-12 cat-text"><p class="lh-sm text-end">Kirim pertanyaan anda ke Contact Center kami</p></div>
                    <div class="contact-form">
                            @if(Session::has('success'))
                                <div class="alert alert-primary">
                                    {{Session::get('success')}}
                                </div>
                            @endif
                            <form action="{{ route('contact.store') }}" method="POST" id="contactUSForm">
                            {{ csrf_field() }}

                                <div class="row">
                                    <div class="col-md-6 pb-1">
                                        <div class="single-form">
                                            <input type="text" class="form-control form-control-lg" id="name" name="name" placeholder="Name*" value="{{ old('name') }}" required>
                                            @if ($errors->has('name'))
                                            <span class="text-danger">{{ $errors->first('name') }}</span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-md-6 py-1">
                                        <div class="single-form">
                                            <input type="text" class="form-control form-control-lg" id="email" name="email" placeholder="Email*" value="{{ old('email') }}" required>
                                            @if ($errors->has('email'))
                                            <span class="text-danger">{{ $errors->first('email') }}</span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-md-6 py-1">
                                        <div class="single-form">
                                            <input type="text" class="form-control form-control-lg" id="subject" name="subject" placeholder="Subject*" value="{{ old('subject') }}" required>
                                            @if ($errors->has('subject'))
                                            <span class="text-danger">{{ $errors->first('subject') }}</span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-md-6 py-1">
                                        <div class="single-form">
                                            <input type="text" class="form-control form-control-lg" id="phone" name="phone" placeholder="Phone No*" value="{{ old('phone') }}" required>
                                            <input type="text" class="form-control form-control-lg" id="websites" name="websites" value="olymplast" hidden>
                                            @if ($errors->has('phone'))
                                            <span class="text-danger">{{ $errors->first('phone') }}</span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-md-12 py-1">
                                        <div class="single-form">
                                            <textarea class="form-control form-control-lg" id="message" name="message" placeholder="Tulis Pesan Disini" required></textarea>
                                            @if ($errors->has('message'))
                                            <span class="text-danger">{{ $errors->first('message') }}</span>
                                            @endif
                                        </div>
                                    </div>
                                    <p class="form-message"></p>
                                    <div class="col-md-12">
                                        <div class="single-form">
                                            <!-- <button type="submit" class="btn btn-dark btn-hover-primary">Kirim Pesan</button> -->
                                            <button class="btn btn-primary btn-lg" type="submit">Submit</button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    
                    
                </div>
            </div>
        </div>
    </div>
</div>
<!-- <div class="kategori section pt-5 mt-5">
    <div class="rounded shadow-sm">    
      <div class="col-12 bg-ol-grey rounded overflow-hidden h-100  position-relative cat-card">
        <div class="position-absolute top-0  p-5 cat-image">
            <div class="contact-map">
                <iframe class="ol-contact-map__iframe" src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d3959.058605911803!2d112.4623557!3d-7.1192072!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2e77fa27c9dc5e41%3A0x3f07169f7937eed1!2sPT.%20Cahaya%20Bintang%20Olympic!5e0!3m2!1sid!2sid!4v1610097432911!5m2!1sid!2sid" frameborder="0" style="border:0;" allowfullscreen=""></iframe>
            </div>
        </div>
        <div class="position-absolute top-0 end-0 p-5 mt-3 me-2 cat-border">
    </div>
    <div class="position-absolute bottom-0 end-0 px-5 pb-4 text-end">
        <div class="h2 text-uppercase fw-bold cat-title col-lg-7 offset-lg-5 col-12 ps-5 text-end">Contact Us</div>
            <div class="col-6 offset-6 cat-text"><p class="lh-sm ">Kirim pertanyaan anda ke Contact Center kami</p></div>
                <div class="contact-form">
                    <form id="contact-form" action="assets/php/contact.php" method="post">
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="single-form">
                                    <input type="text" name="name" placeholder="Name*">
                                </div>
                            </div>
                            <div class="col-lg-12">
                                <div class="single-form">
                                    <input type="email" name="email" placeholder="Email*">
                                </div>
                            </div>
                            <div class="col-lg-12">
                                <div class="single-form">
                                    <input type="text" name="subject" placeholder="Subject">
                                </div>
                            </div>
                            <div class="col-lg-12">
                                <div class="single-form">
                                    <input type="text" name="phone" placeholder="Phone No">
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="single-form">
                                    <textarea name="message" placeholder="Write your comments here"></textarea>
                                </div>
                            </div>
                            <p class="form-message"></p>
                            <div class="col-md-12">
                                <div class="single-form">
                                    <button type="submit" class="btn btn-dark btn-hover-primary">Submit Review</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
                
            </div>
        </div>    
    </div>
</div> -->

<!-- <div class="section page-banner-section vh-50 desaturate color-over-white" style="background-image: url(https://asset.olymplast.co.id/BANNER/olympic-tentang.webp)">
        <div class="container zindex-tooltip">
            <div class="page-banner-content ">
                <h2 class="title text-shadow-sm">Contact Us</h2>

                <ul class="breadcrumb">
                    <li><a href="index.html">Home</a></li>
                    <li class="active">Contact Us</li>
                </ul>
            </div>
        </div>
    </div>
    <div class="section section-padding">
        <div class="container">
            <div class="contact-wrapper">
                <div class="row gx-0">
                    <div class="col-lg-4">
                        <div class="contact-info text-ol-dsblue p-5">
                            <h2 class="h1 pt-5">Kontak kami</h2>
                            <p>Kirim pertanyaan anda ke Call Center kami</p>
                            <div class="contact-info-items text-start pt-5">

                                <div class="single-contact-info">
                                    <div class="info-icon">
                                        <i class="pe-7s-call"></i>
                                    </div>
                                    <div class="info-content">
                                        <p><a href="tel:+6281315115111">+62 813-1511-5111</a></p>
                                    </div>
                                </div>

                                <div class="single-contact-info">
                                    <div class="info-icon">
                                        <i class="pe-7s-mail"></i>
                                    </div>
                                    <div class="info-content">
                                        <p><a href="mailto:contact@olympicfurniture.co.id"> contact@olympicfurniture.co.id</a></p>
                                    </div>
                                </div>

                                <div class="single-contact-info">
                                    <div class="info-icon">
                                        <i class="pe-7s-map-marker"></i>
                                    </div>
                                    <div class="info-content">
                                        <p>Jl. Raya Gresik-Lamongan Km.40 Desa Rejosari, Kec. Deket, Lamongan 62291</p>
                                    </div>
                                </div>

                            </div>
                            

                        </div>
                    </div>
                    <div class="col-lg-8">
                        <div class="contact-form">
                            <form id="contact-form" action="assets/php/contact.php" method="post">
                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="single-form">
                                            <input type="text" name="name" placeholder="Name*">
                                        </div>
                                    </div>
                                    <div class="col-lg-12">
                                        <div class="single-form">
                                            <input type="email" name="email" placeholder="Email*">
                                        </div>
                                    </div>
                                    <div class="col-lg-12">
                                        <div class="single-form">
                                            <input type="text" name="subject" placeholder="Subject">
                                        </div>
                                    </div>
                                    <div class="col-lg-12">
                                        <div class="single-form">
                                            <input type="text" name="phone" placeholder="Phone No">
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="single-form">
                                            <textarea name="message" placeholder="Write your comments here"></textarea>
                                        </div>
                                    </div>
                                    <p class="form-message"></p>
                                    <div class="col-md-12">
                                        <div class="single-form">
                                            <button type="submit" class="btn btn-dark btn-hover-primary">Submit Review</button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="contact-map">
    <iframe class="ol-contact-map__iframe" src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d3959.058605911803!2d112.4623557!3d-7.1192072!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2e77fa27c9dc5e41%3A0x3f07169f7937eed1!2sPT.%20Cahaya%20Bintang%20Olympic!5e0!3m2!1sid!2sid!4v1610097432911!5m2!1sid!2sid" frameborder="0" style="border:0;" allowfullscreen=""></iframe>
    </div> -->

    
    @stop

    @section('script')
    
    <script type="application/ld+json">
    {
    "@context": "https://schema.org/",
    "@type": "BreadcrumbList",
    "itemListElement": [
                {
                "@type": "ListItem",
                "position": "1",
                "name": "Home",
                "item": "https://olymplast.co.id/"
                },
                {
                "@type": "ListItem",
                "position": "2",
                "name": "Contact",
                "item": "https://olymplast.co.id/contact/"
                }
            ]
        }
    </script>
    <script type="application/ld+json">
    {
        "@type": "Organization",
        "@id": "https://olymplast.co.id/#organization",
        "name": "PT. Cahaya Bintang Plastindo",
        "url": "https://olymplast.co.id/#organization",
        "sameAs": [
            "https://www.facebook.com/olymplast",
            "https://www.instagram.com/olymplast",
            "https://www.youtube.com/c/olympicfurnituregroup_indonesia",
            "https://www.linkedin.com/company/olympic-group-pt-graha-multi-bintang/"
        ],
        "logo": {
            "@type": "ImageObject",
            "@id": "https://olymplast.co.id/#logo",
            "inLanguage": "id-id",
            "url": "https://asset.olymplast.co.id/BRAND/OLYMPLAST/fav_Icon_Olymplast.webp",
            "width": 300,
            "height": 186,
            "caption": "PT. Cahaya Bintang Plastindo"
        },
        "image": {
            "@id": "hhttps://olymplast.co.id/#logo"
        }
    }
    </script>

        @endsection