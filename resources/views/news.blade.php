@extends('template')

@section('m_title')
{{$meta->meta_title}}
@stop

@section('m_desc')
{{$meta->meta_desc}}
@stop

@section('m_type')
website
@stop

@section('m_author')
Olympic
@stop

@section('m_keywords')
{{$meta->keyword}}
@stop

@section('m_image')
https://asset.olymplast.co.id/BRAND/OLYMPIC/ASSET/og-furniture.png
@stop

@if($mode === 'all')
    @section('m_canonical')
    https://olymplast.co.id/blog
    @stop
@else
    @section('m_canonical')
    https://olymplast.co.id/blog/{{$mode}}/{{$meta->slug}}
    @stop
@endif

@section('m_robots')
all
@stop


@section('content')
    <!-- <div class="section page-banner-section vh-25 desaturate color-over-white" style="background-image: url({{asset('images/page-banner.jpg')}});">
        <div class="container">
            <div class="page-banner-content">
                <ul class="breadcrumb">
                    <li><a href="index.html">Home</a></li>
                    <li class="active">News</li>
                </ul>
            </div>
        </div>
    </div> -->
<div class="blank-header d-block">

</div>
@if($mode === 'all')
<div class="section">
    <div class="container-xxl">
        <div class="px-lg-4 px-2 sliders  ">
            <div class="swiper-container shadow-sm rounded">
                <div class="swiper-wrapper">
                @foreach($sliders as $slider)
                    <div class="swiper-slide">
                        <!-- <div class="d-lg-none d-block position-relative">
                            <img class="img-fluid w-100 " src="https://asset.olymplast.co.id/NEWS/{{($slider->image)}}" alt="" title="">
                            <div class="position-absolute w-100 pb-5 bottom-0">
                                <div class=" px-5 pb-1 text-start">
                                    <p class=" btn btn-primary btn-sm">
                                        {{($slider->name_id)}}
                                    </p>
                                    <p class="h1 fw-bold  col-10 text-black text-start">
                                        {{($slider->title)}}
                                    </p>
                                </div>
                            </div>
                        </div> -->
                        
                        <div class="w-100  position-relative height-blog" style="background: url(https://asset.olymplast.co.id/NEWS/{{$slider->image}}) rgba(0, 0, 0, 0.35); background-size:cover; background-position:center center; background-blend-mode: overlay;">
                            <div class="position-absolute w-100 pb-5 bottom-0">
                                <div class=" px-lg-5 px-3 pb-1 text-start">
                                        @foreach($ncategories->where('uni_news_id', $slider->id) as $nctg) 

                                            <a href="{{url('blog/cat')}}/{{$nctg->slug}}" class=" btn btn-primary btn-sm">
                                                {{$nctg->name_id}}
                                            </a>
                                        @endforeach 
                                    <!-- <p class=" btn btn-primary btn-sm">

                                    {{($slider->name_id)}}
                                    </p> -->
                                    <a href="{{url('blog/')}}/{{$slider->slug}}">
                                        <p class="slider-title fw-bold  col-lg-10 col-12 text-white text-start">
                                            {{($slider->title)}}
                                        </p>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach 
                </div>
                <div class="swiper-pagination"></div>

            </div>
        </div>
    
    </div>
</div>
@endif

<div class="section main-banner position-relative">
  <div class="swiper shadow">
    <div class="swiper-wrapper">
      @foreach($banner_pc as $b_p)
        <div class="swiper-slide d-lg-block d-none">
          <a href="{{ ($b_p->external_link) }}">
            <img class="img-fluid" src="https://asset.olymplast.co.id/BANNER/{{($b_p->image_desktop)}}" alt="" title="">
          </a>
        </div>
      @endforeach 
      @foreach($banner_mobile as $b_m)
        <div class="swiper-slide d-block d-lg-none">
          <a href="{{ ($b_m->external_link) }}">
            <img class="img-fluid " src="https://asset.olymplast.co.id/BANNER/{{($b_m->image_mobile)}}" alt="" title="">
          </a>
        </div>
      @endforeach
    </div>
  </div>
</div>
<div class="section ">
    <div class="container-xxl position-relative kategori px-0">
        <div class="px-lg-4 px-4 position-relative ">
            @if($mode === 'tag')
                <div class="h2  fw-bold cat-title col-lg-12 col-12 pt-3  fw-bold  border-bottom "><span class="d-inline-block mb-0 pb-3 text-ol-dsblue " style="border-bottom:1px solid black;">Tag : {{$meta->page_name}} </span></div>
            @elseif($mode === 'cat')
                <div class="h2  fw-bold cat-title col-lg-12 col-12 pt-3  fw-bold  border-bottom "><span class="d-inline-block mb-0 pb-3 text-ol-dsblue " style="border-bottom:1px solid black;">Kategori : {{$meta->page_name}} </span></div>
            @else
                <div class="swiper-container pt-3 ">
                    <div class="swiper-wrapper pb-3">
                        @foreach($categories as $ctg)
                            <div class="swiper-slide">
                                <a href="{{url('blog/cat')}}/{{$ctg->slug}}" >
                                    <div class="position-relative rounded overflow-hidden">
                                        <img class="img-fluid " style="width:300px" src="https://asset.olymplast.co.id/NEWS/CATEGORY/{{$ctg->image}}" alt="" title="">
                                        <div class="position-absolute bottom-0 text-white text-center col-12  {{ (request()->is("blog/cat/$ctg->slug")) ? 'bg-ol-lblue fw-bolder' : 'bg-ol-tlblue ' }}">
                                            {{$ctg->name_id}}
                                        </div>
                                    </div>
                                </a>
                            </div>   
                        @endforeach
                    </div>
                    <div class="swiper-scrollbar"></div>
                </div>
            @endif
            <div class="rounded-end bg-ol-lblue swiper-buttons color-ol-white swiper-button-prev justify-content-center d-flex align-items-center" ><i class="fa-solid fa-angle-left"></i></div>
        <div class="rounded-start bg-ol-lblue swiper-buttons color-ol-white swiper-button-next justify-content-center d-flex align-items-center" ><i class="fa-solid fa-angle-right"></i></div>
    </div>
        
</div>
<div class=" section pb-5">
        <!-- <div class="container-lg">
            <div class="col-12 px-3">
                <div class=" rounded-3 overflow-hidden">
                    <img src="https://cdn.morefurniture.id/MORE/custom/Banner-Bawah.webp" alt="product">
                </div>
            </div>
        </div> -->
        <div class="container-xxl pt-5">
            <div class="px-lg-4 px-2">
                <div class="row g-4">
                    
                    @foreach($news as $nws)

                    <div class="col-lg-4 ">
                        <div class="d-flex flex-column rounded overflow-hidden bloghover bg-ol-white">
                            <div class="h-100 col-12 outer-glow rounded">
                                <a href="{{url('blog/')}}/{{$nws->slug}}">
                                    <img class="img-fluid" src="https://asset.olymplast.co.id/NEWS/{{($nws->image)}}" alt="{{($nws->image_alt)}}" title="{{($nws->image_title)}}">
                                </a>
                            </div>
                            <div class="col-12 px-lg-2">
                                <a href="{{url('blog/')}}/{{$nws->slug}}">
                                    <p class="h6 text-ol-black text-start mb-0 pt-3">{{($nws->title)}}</p>
                                </a>
                            </div>
                            
                            <div class="col-12 text-ol-grey text-start pb-2 px-lg-2">
                            @if($nws->name_id!=null)
                                <span class="h7"></span><span class="h7 fw-bold">{{($nws->name_id)}} •</span>

                            @endif
                                <span class="h7 ">{{ date('j M Y', strtotime($nws->date)) }}</span>

                            </div>
                            <!-- <div class="d-flex px-3">
                                <div class=" d-flex pe-2">
                                    <div class="col flex-grow-1 align-self-center">
                                    <i class="fa-solid fa-calendar-days text-ol-grey "></i>
                                    </div>
                                    <div class="col d-flex flex-column  p-2">
                                    <div class="border-end pe-2" >
                                        <div class="row " >
                                        <span class="h8  text-ol-grey">{{ jdmonthname(date('m', strtotime($nws->date)),0) }}</span>
                                        </div>
                                        <div class="row">
                                        <span class="mb-0 h6 lh-05 text-ol-black">{{ date('j', strtotime($nws->date)) }}</span>
                                        </div>
                                        <div class="row">
                                        <span class="h8  text-ol-grey">{{ date('Y', strtotime($nws->date)) }}</span>
                                        </div>
                                    </div>
                                    </div>
                                </div>
                                <div class="ms-auto flex-grow-1 align-self-center">
                                    <p class="h6 text-ol-black text-start">{{($nws->title)}}</p>
                                </div>
                            </div> -->
                        </div>
                    </div>




                        
                    @endforeach

                    
                </div>
                <div class="pt-5 d-flex justify-content-center page-pagination">
                    {{$news->links()}}
                </div>
            </div>
        </div>
    </div>
@stop

@section('script')
    <script>
    var swiper = new Swiper('.kategori .swiper-container', {
      spaceBetween: 10,
      slidesPerColumn: 2,
      slidesPerView: 4,
      loop: true,
      slidesPerColumnFill: 'column',
      breakpoints: {
            960: {
            slidesPerView: 4,
            spaceBetween: 8
            },
            720: {
            slidesPerView: 3,
            spaceBetween: 6
            },
            540: {
            slidesPerView: 2,
            spaceBetween: 4
            },
            320: {
            slidesPerView: 2,
            spaceBetween: 2
            },
        },
      navigation: {
            nextEl: '.kategori .swiper-button-next',
            prevEl: '.kategori .swiper-button-prev',
        },
        
    });
    var slider7 = new Swiper('.slider-7 .swiper-container', {
        loop: true,
        slidesPerView: 7,
        spaceBetween: 30,
        navigation: {
            nextEl: '.slider-7 .swiper-button-next',
            prevEl: '.slider-7 .swiper-button-prev',
        },
    });
    var slider5 = new Swiper('.slider-5 .swiper-container', {
        spaceBetween: 10,
        slidesPerView: 5,
        loop: true,
        scrollbar: {
            el: ".slider-5 .swiper-scrollbar",
            hide: true,
        },
        navigation: {
            nextEl: '.slider-5 .swiper-button-next',
            prevEl: '.slider-5 .swiper-button-prev',
        },
        breakpoints: {
            0: {
                slidesPerView: 2,
            },
            576: {
                slidesPerView: 2.5,
            },
            768: {
                slidesPerView: 4,
            },
            992: {
                slidesPerView: 5,
            }
        }
    });
    var sliders = new Swiper('.sliders .swiper-container', {
        speed: 600,
        loop: true,
        spaceBetween: 0,
        slidesPerView: 1,
        pagination: {
            el: ".swiper-pagination",
        },
    }); 
    </script>
    @if($mode === 'tag')
        <script type="application/ld+json">
            {
            "@context": "https://schema.org/",
            "@type": "BreadcrumbList",
            "itemListElement": [
                    {
                    "@type": "ListItem",
                    "position": "1",
                    "name": "Home",
                    "item": "https://olymplast.co.id/"
                    },
                    {
                    "@type": "ListItem",
                    "position": "2",
                    "name": "Blog",
                    "item": "https://olymplast.co.id/blog/"
                    },
                    {
                    "@type": "ListItem",
                    "position": "3",
                    "name": "{{$meta->page_name}}",
                    "item": "https://olymplast.co.id/blog/tag/{{$meta->slug}}"
                    }
                ]
            }
        </script>
    @elseif($mode === 'cat')
        <script type="application/ld+json">
            {
            "@context": "https://schema.org/",
            "@type": "BreadcrumbList",
            "itemListElement": [
                    {
                    "@type": "ListItem",
                    "position": "1",
                    "name": "Home",
                    "item": "https://olymplast.co.id/"
                    },
                    {
                    "@type": "ListItem",
                    "position": "2",
                    "name": "Blog",
                    "item": "https://olymplast.co.id/blog/"
                    },
                    {
                    "@type": "ListItem",
                    "position": "3",
                    "name": "{{$meta->page_name}}",
                    "item": "https://olymplast.co.id/blog/cat/{{$meta->slug}}"
                    }
                ]
            }
        </script>
    @else
    <script type="application/ld+json">
        {
        "@context": "https://schema.org/",
        "@type": "BreadcrumbList",
        "itemListElement": [
                {
                "@type": "ListItem",
                "position": "1",
                "name": "Home",
                "item": "https://olymplast.co.id/"
                },
                {
                "@type": "ListItem",
                "position": "2",
                "name": "Blog",
                "item": "https://olymplast.co.id/blog/"
                }
            ]
        }
    </script>
    @endif
    <script type="application/ld+json">
    {
        "@type": "Organization",
        "@id": "https://olymplast.co.id/#organization",
        "name": "PT. Cahaya Bintang Plastindo",
        "url": "https://olymplast.co.id/#organization",
        "sameAs": [
            "https://www.facebook.com/olymplast",
            "https://www.instagram.com/olymplast",
            "https://www.youtube.com/c/olympicfurnituregroup_indonesia",
            "https://www.linkedin.com/company/olympic-group-pt-graha-multi-bintang/"
        ],
        "logo": {
            "@type": "ImageObject",
            "@id": "https://olymplast.co.id/#logo",
            "inLanguage": "id-id",
            "url": "https://asset.olymplast.co.id/BRAND/OLYMPLAST/fav_Icon_Olymplast.webp",
            "width": 300,
            "height": 186,
            "caption": "PT. Cahaya Bintang Plastindo"
        },
        "image": {
            "@id": "hhttps://olymplast.co.id/#logo"
        }
    }
    </script>
@stop