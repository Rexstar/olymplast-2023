@extends('template')

@section('m_title')
{{$meta->meta_title}}
@stop

@section('m_desc')
{{$meta->meta_desc}}
@stop

@section('m_type')
website
@stop

@section('m_author')
Olymplast
@stop

@section('m_keywords')
{{$meta->keyword}}
@stop

@section('m_image')
https://asset.olymplast.co.id/BRAND/OLYMPIC/ASSET/og-furniture.png
@stop

@section('m_canonical')
https://olymplast.co.id/kol
@stop

@section('m_robots')
all
@stop
@section('content')
<div class="w-100 vh-100px d-lg-none d-block" >
</div>
<div class="content pt-5">
    <!-- <div class="d-lg-none" >
        <a href="#pocket">
            <img class="img-fluid" src="https://cdn.olymplast.co.id/BRAND/MICROSITE/OLYMPIC/IMAGES/banner-sb-mobile-1.webp" alt="" title="">
        </a>
    </div>
    <div class="d-none d-lg-block">
        <a href="#pocket">
            <img class="img-fluid" src="https://cdn.olymplast.co.id/BRAND/MICROSITE/OLYMPIC/IMAGES/banner-sb-1.webp" alt="" title="">
        </a>
    </div>
    <div class="d-lg-none p-3" >
        
            <img class="img-fluid" src="https://cdn.olymplast.co.id/BRAND/MICROSITE/OLYMPIC/IMAGES/header-sb-text-mobile-1.webp" alt="" title="">
        
    </div> -->
    <div class="text-center fw-bold h2 section-title pt-5">
    Testimoni Pelanggan
  </div>
    <div class="container-xxl py-3">
        <div class="row g-3 px-lg-4 px-2" data-masonry='{"percentPosition": true }'>
        @foreach($kol as $product)
            <div class="col-lg-2 col-6 py-1">
            <div class="card ">

                <picture>
                    <img class="img-fluid " src="https://asset.olymplast.co.id/ENDORSEMENT/{{$product->image_content}}" alt="" >
                </picture>
                <div class="card-body">
                    <div class="card-title h5">{{$product->name}}</div>
                    <div class=""><span class="h7">{{$product->ig}}</span></div>
                </div>

                    
                        
            </div>
        </div>
        @endforeach
    </div>
</div>
</div>


@endsection
@section('script')
<script src="https://cdn.jsdelivr.net/npm/masonry-layout@4.2.2/dist/masonry.pkgd.min.js" integrity="sha384-GNFwBvfVxBkLMJpYMOABq3c+d3KnQxudP/mGPkzpZSTYykLBNsZEnG2D9G/X/+7D" crossorigin="anonymous" async></script>

<script type="application/ld+json">
    {
    "@context": "https://schema.org/",
    "@type": "BreadcrumbList",
    "itemListElement": [
            {
            "@type": "ListItem",
            "position": "1",
            "name": "Home",
            "item": "https://olymplast.co.id/"
            },
            {
            "@type": "ListItem",
            "position": "2",
            "name": "About",
            "item": "https://olymplast.co.id/about/"
            }
        ]
    }
    </script>
    <script type="application/ld+json">
    {
        "@type": "Organization",
        "@id": "https://olymplast.co.id/#organization",
        "name": "PT. Cahaya Bintang Plastindo",
        "url": "https://olymplast.co.id/#organization",
        "sameAs": [
            "https://www.facebook.com/olymplast",
            "https://www.instagram.com/olymplast",
            "https://www.youtube.com/c/olympicfurnituregroup_indonesia",
            "https://www.linkedin.com/company/olympic-group-pt-graha-multi-bintang/"
        ],
        "logo": {
            "@type": "ImageObject",
            "@id": "https://olymplast.co.id/#logo",
            "inLanguage": "id-id",
            "url": "https://asset.olymplast.co.id/BRAND/OLYMPLAST/fav_Icon_Olymplast.webp",
            "width": 300,
            "height": 186,
            "caption": "PT. Cahaya Bintang Plastindo"
        },
        "image": {
            "@id": "hhttps://olymplast.co.id/#logo"
        }
    }
    </script>

    @stop