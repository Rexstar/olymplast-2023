@extends('template')

@section('content')
<!-- <div class="section page-banner-section vh-25 desaturate color-over-white" style="background-image: url({{asset('images/page-banner.jpg')}});">
    <div class="container">

        <div class="page-banner-content">
            <ul class="breadcrumb">
                <li><a href="index.html">Home</a></li>
                <li class="active">Shop Page</li>
            </ul>
        </div>

    </div>
</div> -->
<div class="w-100 vh-100px d-block" >
</div>
<div class="section pt-3">
    <div class="container-xxl">
        <div class="row flex-row-reverse px-lg-4 px-2">
            <div class="col-lg-10">
                <!-- Shop top Bar Start -->
                <div class="shop-top-bar">
                    <div class="shop-text">
                    <p>Menampilkan <span class="fw-bold">{{ $produk->total() }}</span> produk untuk pencarian <span class="fw-bold">"{{ $req }}"</span></p>
                    </div>
                </div>
                <!-- Shop top Bar End -->

                    <div class="tab-pane fade show active" id="grid">
                        <!-- Shop Product Wrapper Start -->
                        <div class="shop-product-wrapper">
                            <div class="row g-4">
                                @foreach($produk as $prod)
                                <div class="col-lg-4 col-sm-6 ">
                                    <div class="outer-glow-sm rounded overflow-hidden">
                                        <div class="">
                                            <!-- <a href="{{url('product/')}}/{{$prod->slug}}">
                                            <div class="h-list-produk">
                                                <img class="img-fluid" src="https://asset.olymplast.co.id/{{$prod->images}}" alt="product">
                                            </div>
                                            <div class="product-content align-middle">
                                                <p class="h5 title text-ol-grey judul-produk">{{$prod->judul_brand}}</p>
                                            </div>
                                            </a> -->
                                            <a href="{{url('product')}}/{{$prod->slug}}">
                                                <div class="single-product d-flex justify-content-center">
                                                    <img class="h-100" src="https://asset.olymplast.co.id/{{$prod->images}}" alt="" title="">
                                                </div>
                                            <div class="px-4 pb-4">
                                                <div class=" text-start">
                                                <p class="h6 text-ol-black cat-text">{{($prod->category_name)}}</p>
                                                <p class="h5  fw-bold text-ol-black">{{($prod->judul_brand)}}</p>
                                                </div>
                                                <div class="card-border"></div>
                                            </div>
                                            </a>
                                            <!-- <ul class="product-meta">
                                                <li><a class="action" data-bs-toggle="modal" class="detail-prod" value="{{$prod->id}}" data-bs-target="#quickView" href="#"><i class="pe-7s-look"></i></a></li>
                                            </ul> -->
                                        </div>
                                    </div>
                                    <!-- Single Product End -->
                                </div>
                                <!-- <div class="col-lg-4 col-sm-6">
                                    <div class="single-product ">
                                        <a href="{{url('product/')}}/{{$prod->slug}}">
                                        <div class="h-list-produk">
                                            <img class="img-fluid" src="https://asset.olymplast.co.id/{{$prod->images}}" alt="product">
                                        </div>
                                        <div class="product-content align-middle">
                                            <h4 class=" title text-truncate text-ol-lsblue">{{$prod->name}}</h4>
                                        </div>
                                        </a>
                                    </div>
                                </div> -->
                                @endforeach
                                
                            </div>
                        </div>
                        <!-- Shop Product Wrapper End -->
                    </div>


                <div class="page-pagination">
                    {{$produk->links()}}
                </div>
                <!-- Page pagination End -->
            </div>
            <div class="col-lg-2">
                <!-- Sidebar Start -->
                <div class="sidebar">
                    <div class="sidebar-widget text-start">
                        <h4 class="widget-title text-ol-dgrey">Kategori</h4>
                        <div class=" ">
                                <div class=" h6 pt-2 fw-bold"><a class="text-ol-grey" href="{{url('product')}}">Semua</a></div>
                            @foreach($parent_cat as $pctg)
                                <div class=" h6 pt-2 fw-bold"><a class="text-ol-grey" href="{{url('category')}}/{{$pctg->slug}}">{{$pctg->name}}</a></div>
                                <ul>
                                @foreach($child_cat->where('uni_categories_id', $pctg->id) as $cctg)
                                    <li><span><a class="text-ol-dgrey h6" href="{{url('category')}}/{{$cctg->slug}}">{{$cctg->name}}</a><span></li>
                                @endforeach
                                </ul>
                            @endforeach
                        </div>
                    </div>
                    <!-- Sidebar Widget End -->
                   
                    <!-- Sidebar Widget Start -->
                    <!-- <div class="sidebar-widget text-start">
                        <h4 class="widget-title">Tags</h4>
                        <div class="widget-tags">
                            <ul class="tags-list">
                                <li><a href="#">Clothing</a></li>
                                <li><a href="#">Accessories</a></li>
                                <li><a href="#">For Men</a></li>
                                <li><a href="#">Women</a></li>
                                <li><a href="#">Fashion</a></li>
                            </ul>
                        </div>
                    </div> -->
                    <!-- Sidebar Widget End -->
                </div>
                <!-- Sidebar End -->
            </div>
        </div>
    </div>
</div>

    <div class="modal fade" id="quickView">
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                <div class="modal-body">

                    <div class="row">
                        <div class="col-lg-6">

                            <!-- Quick View Images Start -->
                            <div class="">

                                <!-- Quick Gallery Images Start -->
                                <div class="quick-gallery-images">
                                    <div class="swiper-container">
                                        <div class="swiper-wrapper">
                                            <div class="swiper-slide">
                                                <div class="single-img">
                                                    <img src="{{asset('images/product-details/product-details-1.jpg')}}" alt="Product Image">
                                                </div>
                                            </div>
                                            <div class="swiper-slide">
                                                <div class="single-img">
                                                    <img src="{{asset('images/product-details/product-details-2.jpg')}}" alt="Product Image">
                                                </div>
                                            </div>
                                            <div class="swiper-slide">
                                                <div class="single-img">
                                                    <img src="{{asset('images/product-details/product-details-3.jpg')}}" alt="Product Image">
                                                </div>
                                            </div>
                                            <div class="swiper-slide">
                                                <div class="single-img">
                                                    <img src="{{asset('images/product-details/product-details-4.jpg')}}" alt="Product Image">
                                                </div>
                                            </div>
                                            <div class="swiper-slide">
                                                <div class="single-img">
                                                    <img src="{{asset('images/product-details/product-details-5.jpg')}}" alt="Product Image">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- Quick Gallery Images End -->

                                <!-- Quick Gallery Thumbs Start -->
                                
                                <!-- Quick Gallery Thumbs End -->

                            </div>
                            <!-- Quick View Images End -->

                        </div>
                        <div class="col-lg-6">

                            <!-- Quick View Description Start -->
                            <div class="quick-view-description text-start">
                                <h4 class="product-name text-start">Products Name </h4>
                                <div class="price">
                                    <span class="sale-price">Eira Series</span>
                                    <input type="text"  id="testtt">
                                </div>
                                <!-- <div class="review-wrapper">
                                    <div class="review-star">
                                        <div class="star" style="width: 80%;"></div>
                                    </div>
                                    <p>( 1 Customer Review )</p>
                                </div> -->
                                <div class="product-color">
                                    <span class="lable">Color:</span>
                                    <ul>
                                        <li>
                                            <input type="radio" name="colors" id="color1">
                                            <label for="color1"><span class="color-blue"></span></label>
                                        </li>
                                        <li>
                                            <input type="radio" name="colors" id="color2">
                                            <label for="color2"><span class="color-gray"></span></label>
                                        </li>
                                        <li>
                                            <input type="radio" name="colors" id="color3">
                                            <label for="color3"><span class="color-dark-blue"></span></label>
                                        </li>
                                        <li>
                                            <input type="radio" name="colors" id="color4">
                                            <label for="color4"><span class="color-gray-dark"></span></label>
                                        </li>
                                    </ul>
                                </div>

                                <p>Meja Sofa Plastik Olymplast Sofa Table Motif Rotan Plastik cocok untuk diruang tamu atau teras rumah, warna. Pilihan warna beige, coklat, putih dan cream </p>

                               

                                <div class="product-info">
                                    <!-- <div class="single-info">
                                        <span class="lable">SKU:</span>
                                        <span class="value">Ch-256xl</span>
                                    </div>
                                    <div class="single-info">
                                        <span class="lable">Categories:</span>
                                        <span class="value"><a href="#">Office,</a> <a href="#">Home</a></span>
                                    </div>
                                    <div class="single-info">
                                        <span class="lable">tag:</span>
                                        <span class="value"><a href="#">Furniture</a></span>
                                    </div> -->
                                    <div class="single-info">
                                        <span class="lable">Share:</span>
                                        <ul class="social">
                                            <li><a href="#"><i class="fa fa-facebook-f"></i></a></li>
                                            <li><a href="#"><i class="fa fa-dribbble"></i></a></li>
                                            <li><a href="#"><i class="fa fa-pinterest-p"></i></a></li>
                                            <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                                            <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="quick-gallery-thumbs pt-5">
                                    <div class="swiper-container">
                                        <div class="swiper-wrapper">
                                            <div class="swiper-slide">
                                                <img src="{{asset('images/product-details/product-details-1.jpg')}}" alt="Product Thumbnail">
                                            </div>
                                            <div class="swiper-slide">
                                                <img src="{{asset('images/product-details/product-details-2.jpg')}}" alt="Product Thumbnail">
                                            </div>
                                            <div class="swiper-slide">
                                                <img src="{{asset('images/product-details/product-details-3.jpg')}}" alt="Product Thumbnail">
                                            </div>
                                            <div class="swiper-slide">
                                                <img src="{{asset('images/product-details/product-details-4.jpg')}}" alt="Product Thumbnail">
                                            </div>
                                            <div class="swiper-slide">
                                                <img src="{{asset('images/product-details/product-details-5.jpg')}}" alt="Product Thumbnail">
                                            </div>
                                        </div>
                                    </div>
                                    <!-- Add Arrows -->
                                    <div class="swiper-button-prev"><i class="pe-7s-angle-left"></i></div>
                                    <div class="swiper-button-next"><i class="pe-7s-angle-right"></i></div>
                                </div>
                            </div>
                            <!-- Quick View Description End -->

                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
    @endsection

    @section('script')

    <script>
        let id = this.value;
        link = '<?= url("/product/api/") ?>'+'/'+id;

            $(".detail-prod").keyup(function() {
                let id = $(this).val();
                $.ajax({
                    url: "<?= url('/product/api/') ?>" + '/' + id,
                    method: 'GET',
                    success: function (data) {
                        $("#testtt").val(data);
                    }
                });
            });
    </script>
    @endsection