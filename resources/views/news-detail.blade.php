@extends('template')

@section('m_title')
{{$detail->title_meta}}
@stop

@section('m_desc')
{{$detail->description_meta}}
@stop

@section('m_type')
article
@stop

@section('m_author')
{{$detail->author}}
@stop

@section('m_keywords')
{{$detail->keyword}}
@stop

@section('m_image')
https://asset.olymplast.co.id/NEWS/{{$detail->image}}
@stop

@section('m_canonical')
https://olymplast.co.id/blog/{{$detail->slug}}
@stop

@section('m_robots')
all
@stop




@section('content')
<div class="w-100 vh-100px d-block" >
</div>

<!-- <div id='bg-blog' class="section w-100 vh-100 position-relative" style="z-index:-1;background: url(https://asset.olymplast.co.id/NEWS/{{$detail->image}}) center center;  background-repeat:no-repeat;">
    <div class="col-8 offset-2 position-absolute bottom-0 text-center pb-5">
        <h1 class="title text-center d-inline-block">{{$detail->title}}</h1>
    </div>
</div> -->
<div class="bg-ol-grey">
    <div class="col-lg-6 offset-lg-3 col-12 container-xxl">
        <img class="img-fluid w-100" src="https://asset.olymplast.co.id/NEWS/{{$detail->image}}" alt="">
    </div>
</div>



<!-- <div class="w-100 vh-100px d-block" >
</div> -->
<!-- <img class="img-fluid" src="https://asset.olymplast.co.id/NEWS/{{$detail->image}}" alt="Blog Details"> -->

    <!-- <div id='bg-blog' class="section w-100 height-blog position-fixed" style="z-index:-1;background: url(https://asset.olymplast.co.id/NEWS/{{$detail->image}}) center center;  background-repeat:no-repeat;">
        <div class="container">

            <div class="page-banner-content">
                
                <ul class="breadcrumb">
                    <li><a href="index.html">Home</a></li>
                    <li class="active">Blog Details</li>
                </ul>
            </div>

        </div>
    </div> -->
    <!-- Page Banner Section End -->

    <!-- Blog Details Section Start -->
    <div class="section section-padding">
        <div class="container col-lg-8 offset-lg-2">
            <div class="row pt-1">
                <div class="order-lg-1 order-2 col-lg-1 col-10 offset-lg-0 offset-1" style="z-index:1">
                    
                    <div class="sticky-top pt-lg-5 col-12">
                        <div class="d-lg-block d-none vh-100px py-5">

                        </div>
                        
                        
                        <div class="col-12 row">
                        <div class="text-center">
                            Share<br><br>
                        </div>
                            <div class="col-lg-12 col-3 text-center">
                                <a href="https://www.facebook.com/sharer/sharer.php?u=https://olymplast.co.id/blog/{{$detail->slug}}" target=”_blank”><i class="opacity h3 ps-1 fa-brands fa-facebook"></i></a>
                            </div>
                            <div class="col-lg-12 col-3 text-center">
                                <a href="https://twitter.com/intent/tweet?text=Yourface&url=https://olymplast.co.id/blog/{{$detail->slug}}" target=”_blank”><i class="opacity h3 ps-1 fa-brands fa-twitter"></i></a>
                            </div>
                            <div class="col-lg-12 col-3 text-center">
                                <a href="https://api.whatsapp.com/send/?phone&text={{$detail->title}}+-++-+https://olymplast.co.id/blog/{{$detail->slug}} " target=”_blank”><i class="opacity h3 ps-1 fa-brands fa-whatsapp"></i></a>
                            </div>
                            <div class="col-lg-12 col-3 text-center">
                                <a href="#" onclick="copyTeks()" > <i class="opacity h4 ps-1 fa-solid fa-link"></i></a><input hidden type="text" class="form-control" id="dataCopy" value="https://olymplast.co.id/blog/{{$detail->slug}}" name="">
                            </div>


                        </div>
                        

                    </div>
                    

                </div>
                <div class="order-lg-2 order-1 col-lg-10 col-12 rounded overflow-hidden bg-ol-white p-4">

                    <!-- Blog Details Wrapper Start -->
                    <div class="blog-details-wrapper text-ol-dsblue ">
                        <nav style="--bs-breadcrumb-divider: '»'; " aria-label="breadcrumb" class="d-flex justify-content-center">
                            <ol class="breadcrumb bg-ol-lblue">
                                <li class="breadcrumb-item"><a class="h-100" href="{{url('/')}}">Home</a></li>
                                <li class="breadcrumb-item"><a class="h-100" href="{{url('blog')}}">Blog</a></li>
                                <li class="breadcrumb-item active" aria-current="page"><a href="{{url('blog')}}/cat/{{$detail->cat_slug}}">{{$detail->cat_slug}}</a></li>
                            </ol>
                        </nav>
                        <h1 class="h1 fw-bold text-center ">{{$detail->title}}</h1>
                        <!-- <img class="img-fluid" src="https://asset.olymplast.co.id/NEWS/{{$detail->image}}" alt="Blog Details"> -->
                        <div class="col-12 px-lg-2 pb-4">
                                <p class="h6 text-ol-black text-center mb-0 pt-3">{{($detail->seo_2)}} • {{ date('j M Y', strtotime($detail->date)) }}</p>
                            </div>
                            <div class="container-xxl slider3 position-relative overflow-hidden">
                                <div class="px-lg-0">
                                    <div class="swiper px-3  overflow-hidden"> 
                                        <div class=" swiper-wrapper position-relative" >
                                            @foreach($products as $new)
                                            <div class="swiper-slide  py-1">
                                            <div class="outer-glow rounded overflow-hidden">
                                                <a href="{{ route('product_detail',['nama'=> $new->slug]) }}">
                                                    <div class="">
                                                        <img class="img-fluid" src="https://asset.olymplast.co.id/{{($new->images)}}" alt="" title="">
                                                        <div class="px-4 pb-4">
                                                            <div class=" text-start">
                                                            <span class="d-block h7 lh-1  text-ol-black cat-text">{{$new->slug_category}}</span>
                                                            <span class="d-block h6  fw-bold text-ol-black">{{$new->judul_brand}}</span>
                                                            </div>
                                                            <div class="card-border"></div>
                                                        </div>
                                                    </div>
                                                
                                                </a>
                                            </div>
                                            </div>
                                            @endforeach
                                        </div>
                                        <div class="rounded-end bg-ol-lblue swiper-buttons color-ol-white swiper-button-prev justify-content-center d-flex align-items-center" ><i class="fa-solid fa-angle-left"></i></div>
                                        <div class="rounded-start bg-ol-lblue swiper-buttons color-ol-white swiper-button-next justify-content-center d-flex align-items-center" ><i class="fa-solid fa-angle-right"></i></div>
                                    </div>
                                </div>
                            </div>
                        <article class="fw-normal text-ol-black">
							{!!html_entity_decode(str_replace('morefurniture.id','olymplast.co.id',$detail->content,))!!}
						</article>
                        
                    </div>
                    @foreach($tags as $tag)
                    <a href="{{url('blog/tag')}}/{{$tag->name}}" class="btn btn-primary btn-sm mb-1">#{{$tag->name}}</a>

                    @endforeach
                    
                </div>
            </div>
        </div>
    </div>
    <div class="blog-next-previous-post">
                        <div class=" slider5 section mt-3">
                            <div class="text-center fw-bold h2 section-title">
                                Artikel Terkait
                            </div>
                            <div class="container-xxl  position-relative overflow-hidden">
                                <div class="px-4">
                                    <div class="swiper px-3  overflow-hidden"> 
                                        <div class=" swiper-wrapper position-relative" >
                                            @foreach($blogs as $new)
                                            <div class="swiper-slide outer-glow rounded overflow-hidden">
                                                <a href="{{url('blog/')}}/{{$new->slug}}">
                                                <div class="d-flex flex-column">
                                                    <div class="h-100 col-12">
                                                    <img class="img-fluid" src="https://asset.olymplast.co.id/NEWS/{{($new->image)}}" alt="" title="">
                                                    </div>
                                                    <div class="d-flex px-3">
                                                    <div class=" d-flex pe-2 py-1 d-none ">
                                                        <!-- <div class="col flex-grow-1 align-self-center">
                                                        <i class="fa-solid fa-calendar-days text-ol-grey "></i>
                                                        </div> -->
                                                        <div class="col d-flex flex-column  pe-2 align-self-center">
                                                            <div class="border-end pe-2" >
                                                                <div class="row " >
                                                                <span class="h8  text-ol-grey">{{ jdmonthname(date('m', strtotime($new->date)),0) }}</span>
                                                                </div>
                                                                <div class="row">
                                                                <span class="mb-0 h6 lh-05 text-ol-black">{{ date('j', strtotime($new->date)) }}</span>
                                                                </div>
                                                                <div class="row">
                                                                <span class="h8  text-ol-grey">{{ date('Y', strtotime($new->date)) }}</span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    
                                                    <div class="ms-auto flex-grow-1 align-self-center pt-2">
                                                        <p class="h6 text-ol-black text-start">{{($new->title)}}</p>
                                                    </div>
                                                    </div>
                                                </div>
                                                </a>
                                            </div>
                                            @endforeach
                                        </div>
                                        <div class="rounded-end bg-ol-lblue swiper-buttons color-ol-white swiper-button-prev justify-content-center d-flex align-items-center" ><i class="fa-solid fa-angle-left"></i></div>
                                        <div class="rounded-start bg-ol-lblue swiper-buttons color-ol-white swiper-button-next justify-content-center d-flex align-items-center" ><i class="fa-solid fa-angle-right"></i></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
    @stop

    @section('script')
    <script type="application/ld+json">
    {
    "@context": "https://schema.org/",
    "@type": "BreadcrumbList",
    "itemListElement": [
            {
            "@type": "ListItem",
            "position": "1",
            "name": "Home",
            "item": "https://olymplast.co.id/"
            },
            {
            "@type": "ListItem",
            "position": "2",
            "name": "Blog",
            "item": "https://olymplast.co.id/blog/"
            },
            {
            "@type": "ListItem",
            "position": "3",
            "name": "{{$detail->title}}",
            "item": "https://olymplast.co.id/blog/{{$detail->slug}}"
            }
        ]
    }
    </script>
    <script type="application/ld+json">
    {
        "@type": "Organization",
        "@id": "https://olymplast.co.id/#organization",
        "name": "PT. Cahaya Bintang Plastindo",
        "url": "https://olymplast.co.id/#organization",
        "sameAs": [
            "https://www.facebook.com/olymplast",
            "https://www.instagram.com/olymplast",
            "https://www.youtube.com/c/olympicfurnituregroup_indonesia",
            "https://www.linkedin.com/company/olympic-group-pt-graha-multi-bintang/"
        ],
        "logo": {
            "@type": "ImageObject",
            "@id": "https://olymplast.co.id/#logo",
            "inLanguage": "id-id",
            "url": "https://asset.olymplast.co.id/BRAND/OLYMPLAST/fav_Icon_Olymplast.webp",
            "width": 300,
            "height": 186,
            "caption": "PT. Cahaya Bintang Plastindo"
        },
        "image": {
            "@id": "hhttps://olymplast.co.id/#logo"
        }
    }
    </script>
    <script type="application/ld+json">
        {
            "@type": "Article",
            "@id": "https://olymplast.co.id/blog/{{$detail->slug}}/#article",
            "isPartOf": {
                "@id": "https://olymplast.co.id/blog/{{$detail->slug}}/#webpage"
            },
            "author": {
                "@type": "Person"
            },
            "headline": "{{$detail->title}}",
            "datePublished": "{{$detail->date}}",
            "dateModified": "{{$detail->date}}",
            "commentCount": 0,
            "mainEntityOfPage": {
                "@id": "https://olymplast.co.id/blog/{{$detail->slug}}/#webpage"
            },
            "publisher": {
                "@type": "Organization",
                "@id": "https://olymplast.co.id/#organization"
            },
            "image": {
                "@id": "https://olymplast.co.id/blog/{{$detail->slug}}/#primaryimage"
            },
            "keywords": "sustainable product",
            "articleSection": "{{$detail->cat_slug}}",
            "inLanguage": "id-id"
        }
        </script>
    <script>
        // const swiper = this.swiper;
        // swiper.loopDestroy();
        // swiper.loopCreate();
        
        var slider5 = new Swiper('.slider5 .swiper', {
        speed: 600,
        slidesPerView: 4,
        spaceBetween: 30,
        loop: true,
        breakpoints: {
            0: {
                slidesPerView: 1.4,
                spaceBetween: 10,
                centerSlides:true,
            },
            576: {
                slidesPerView: 1.4,
                spaceBetween: 10,
                centerSlides:true,

            },
            768: {
                slidesPerView: 3,
                spaceBetween: 35,
            },
            992: {
                slidesPerView: 4,
                spaceBetween: 30,
            }
          },
          navigation: {
            nextEl: '.slider5 .swiper-button-next',
            prevEl: '.slider5 .swiper-button-prev',
        },
    }); 
    var slider3 = new Swiper('.slider3 .swiper', {
        speed: 600,
        loop: true,
        slidesPerView: 3,
        breakpoints: {
            0: {
                slidesPerView: 1.6,
                spaceBetween: 5,
                centerSlides:true,
            },
            576: {
                slidesPerView: 1.6,
                spaceBetween: 5,
                centerSlides:true,
            },
            768: {
                slidesPerView: 2,
                spaceBetween: 35,
            },
            992: {
                slidesPerView: 3,
                spaceBetween: 30,
            }
          },
          navigation: {
            nextEl: '.slider3 .swiper-button-next',
            prevEl: '.slider3 .swiper-button-prev',
        },
    }); 


    window.onscroll = function() {scrollFunction2()};

        function scrollFunction2() {
        if (document.body.scrollTop > 250 || document.documentElement.scrollTop > 250) {
            var element = document.getElementById("bg-blog");
            element.classList.add("opacity0");
            element.classList.remove("opacity1");
        } else {
            var element = document.getElementById("bg-blog");
            element.classList.add("opacity1");
            element.classList.remove("opacity0");
        }
    }
    function copyTeks(){
        var valueText = $("#dataCopy").select().val();
        // document.execCommand("copy");
        navigator.clipboard.writeText(valueText);
        alert("url '" + valueText + "' berhasil di salin" )
    }
    </script>
    @stop