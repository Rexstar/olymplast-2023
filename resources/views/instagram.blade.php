<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Instagram API Status</title>
</head>
<body>
    <div>
        <h1>Instagram API Status</h1>
        <p>{{ $statusMessage }}</p>
    </div>
</body>
</html>
