@extends('template')

@section('m_title')
{{$detail['nama']}} | Olymplast Plastic Furniture
@stop

@section('m_desc')
{{$detail['desc']}}
@stop

@section('m_type')

@stop

@section('m_author')
Olymplast
@stop

@section('m_keywords')

@stop

@section('m_image')
https://asset.olymplast.co.id/{{$detail['gambar'][0]['img']}}
@stop

@section('m_canonical')

@stop

@section('m_robots')
all
@stop

@section('content')
<div class="w-100 vh-100px d-block" >
</div>
<div class="section ">
    <div class="container-xxl ">
        <div class="row ">
            <div class="col-lg-5">
                <div class="product-details-images">
                    <div class="details-gallery-images">
                        <div class="swiper-container">
                            <div class="swiper-wrapper">
                            @foreach($detail['gambar'] as $gmb)
                                <div class="swiper-slide class-{{$gmb['no']}}">
                                    <div class="single-img zoom bg-ol-bg1">
                                        <img src="https://asset.olymplast.co.id/{{$gmb['img']}}" alt="Product Image">
                                        <div class="inner-stuff">
                                            <div class="gallery-item" data-src="https://asset.olymplast.co.id/{{$gmb['img']}}">
                                                <a href="javascript:void(0)">
                                                    <i class="lastudioicon-full-screen"></i>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                    <!-- Details Gallery Images End -->
                    <!-- Details Gallery Thumbs Start -->
                    <div class="details-gallery-thumbs">
                        <div class="swiper-container">
                            <div class="swiper-wrapper">
                            @foreach($detail['gambar'] as $gmb)
                                <div class="swiper-slide ">
                                    <img src="https://asset.olymplast.co.id/{{$gmb['img']}}" alt="{{$detail['nama']}}">
                                </div>
                            @endforeach
                            </div>
                        </div>
                        <!-- Add Arrows -->
                        <div class="rounded-1 bg-ol-lblue swiper-buttons color-ol-white swiper-button-prev justify-content-center d-flex align-items-center" ><i class="fa-solid fa-angle-left"></i></div>
                        <div class="rounded-1 bg-ol-lblue swiper-buttons color-ol-white swiper-button-next justify-content-center d-flex align-items-center" ><i class="fa-solid fa-angle-right"></i></div>
                    </div>
                    <!-- Details Gallery Thumbs End -->
                </div>
                <!-- Product Details Images End -->
            </div>
            <div class="col-lg-7">
                
                <div class="product-details-description text-start">
                <nav style="--bs-breadcrumb-divider: '»'; " aria-label="breadcrumb">
                    <ol class="breadcrumb bg-ol-lblue">
                        <li class="breadcrumb-item"><a class="h-100" href="{{url('/')}}">Home</a></li>
                        <li class="breadcrumb-item active" aria-current="page"><a href="{{url('category')}}/{{$detail['category_slug']}}">{{$detail['category_name']}}</a></li>
                    </ol>
                </nav>
                    <h4 class="product-name">{{$detail['nama']}}</h4>

                    @if (count($varian) > 1)
                    <div class="product-color col-12 pe-lg-5 row">
                        @if ($variant_name)
                            <span class="lable col-3">{{$variant_name->variant_1_name}}</span>
                        @else
                            <span class="lable col-3">Varian</span>
                        @endif
                            <div class="col-12 ">
                            

                                @foreach ($variant as $variants)
                                    <!-- <input onclick="goToPage('class-{{$variants->id}}')" type="radio" class="btn-primary btn-sm" name="btnradio" id="btn-{{$variants->id}}" autocomplete="off" checked>
                                    <label class="btn btn-outline-primary" for="btn-{{$variants->id}}">{{$variants->variant}}</label> -->

                                    <input onclick="goToPage('class-{{$variants->id}}')" type="radio" class="btn btn-check py-1" name="options" id="btn-{{$variants->id}}" autocomplete="off">
                                    <label class="btn btn-primary btn-sm" for="btn-{{$variants->id}}">{{$variants->variant}}</label> 

                                    <!-- <a href="#"  onclick="goToPage('class-{{$variants->id}}')" class="ms-2 btn btn-primary btn-sm px-3 mb-2 ">{{$variants->variant}}</a> -->
                                @endforeach

                            </div>
                    </div>
                    <div class="product-color col-12 pe-lg-5  row pe-1 ">
                    <span class="lable col-12">Spesifikasi</span>

                        <div class="col-12  ps-3">

                        @foreach($varian->groupby('variant_2') as $key => $value)
                            <div class=" col-12 ">
                                @foreach($value as $vals)
                                <div class="row col-12 border-bottom">
                                    <div class="col-lg-3 col-5">
                                    <strong>{{$vals->variant}}</strong>
                                    </div>
                                    <div class="col-lg-9 col-7">
                                    {{$vals->dimensions_width}}cm x {{$vals->dimensions_length}}cm x {{$vals->dimensions_height}}cm
                                    </div>
                                    </div>

                                @endforeach
                            </div>
                        @endforeach
                        </div>
                    </div>

                   
                    @endif

                    @if(count($kelebihan)>0)
                    <div class="product-color col-12 pe-lg-5 row">
                        <span class="lable col-12">Kelebihan</span>
                        <div class="col-12 ps-3 text-ol-black">
                            @foreach($kelebihan as $plus)
                            <div class="row col-12 row">
                                <div class="col-12">
                                <i class="ol-fa fas fa-check pe-3"></i> <span class="text-ol-black">{{$plus->name}}</span>
                                </div>
                            </div>
                            @endforeach

                        </div>
                    </div>
                    @endif
                    <div class="product-color col-12 pe-lg-5 row">
                        <span class="lable col-12">Share</span>
                        <div class="col-12 row ps-3">

                            <div class="row h4 col-12">
                                <div class="col-11">
                                    <a href="https://www.facebook.com/sharer/sharer.php?u=https://olymplast.co.id/product/{{$detail['slug']}}"><i class="fa-brands fa-facebook px-2"></i></a>
                                    <a href="https://api.whatsapp.com/send/?phone&text={{$detail['nama']}}+-++-+https://olymplast.co.id/product/{{$detail['slug']}}"><i class="fa-brands fa-whatsapp px-2"></i></a>
                                    <a href="https://twitter.com/intent/tweet?text=Yourface&url=https://olymplast.co.id/product/{{$detail['slug']}}"><i class="fa-brands fa-twitter px-2"></i></a>
                                    <a href="#" onclick="copyTeks()"> <i class="fa-solid fa-link px-2"></i></a><input hidden type="text" class="form-control" id="dataCopy" value="https://olymplast.co.id/product/{{$detail['slug']}}" name="">
                                </div>
                            </div>
                        </div>
                    </div>


                    <p>{{$detail['desc']}}</p>
                    
                    <!-- <div class="product-meta">
                        <div class="product-quantity d-inline-flex">
                            <button type="button" class="sub">-</i></button>
                            <input type="text" value="1" />
                            <button type="button" class="add">+</button>
                        </div>
                        <div class="meta-action">
                            <button class="btn btn-dark btn-hover-primary">Add To Cart</button>
                        </div>
                        <div class="meta-action">
                            <a class="action" href="#"><i class="pe-7s-like"></i></a>
                        </div>
                        <div class="meta-action">
                            <a class="action" href="#"><i class="pe-7s-shuffle"></i></a>
                        </div>
                    </div> -->
                    
                </div>
                <!-- Product Details Description End -->
            </div>
        </div>
    </div>
</div>
    <!-- Product Details Section End -->

    <!-- Product Details tab Section Start -->
    
    <div class="section section-padding-02">
        <div class="container-xxl">

            <!-- Product Details Tabs Start -->
            <div class="product-details-tabs">
                <ul class="nav justify-content-center">
                    <li><button data-bs-toggle="tab" data-bs-target="#information">Informasi</button></li>
                    <li><button class="active" data-bs-toggle="tab" data-bs-target="#description">Detail</button></li>
                </ul>

                <div class="tab-content">
                    <div class="tab-pane fade" id="information">
                        <!-- Information Content Start -->
                        <div class="information-content">
                        @foreach ($video as $value)
                        <div class="col-6 offset-3">
                        <iframe style="height:422px" class="w-100 iframe-youtube" src="https://www.youtube.com/embed/{{ $value->link}}?rel=0" allow="fullscreen;">
                        </iframe><br>
                        </div>
                        @endforeach
                            <div class="g-3 row pt-3 d-flex justify-content-center">

							@foreach($p_detail as $pdetail)
                                @if($pdetail->type === 'PRODUK-INFO')
                                    <div class='col-lg-4 mt-3'>
                                        <img class='img-fluid' src='https://asset.olymplast.co.id/{{$pdetail->path}}/{{$pdetail->filename_webp}}'>
                                    </div>
                                    @php
                                        $found = true; // Set found to true if at least one image is found
                                    @endphp
                                @endif
                            @endforeach
                            @if(!isset($found))
                                @php
                                    for($i = 1; $i <= 13; $i++) {
                                        $external_link = 'https://cdn.olymplast.co.id/'.$detail['path'].'/pi-'.$i.'.webp';
                                        if (@getimagesize($external_link)) {
                                            echo "<div class='col-lg-4 col-md-6 col-12 mt-3'><img class='img-fluid' src='https://cdn.olymplast.co.id/".$detail['path']."/pi-".$i.".webp'></div>";
                                        }
                                    }
                                @endphp
                            @endif

                            </div>
                        </div>
                        <!-- Information Content End -->
                    </div>
                    <div class="tab-pane fade show active" id="description">
                        <!-- Description Content Start -->
                        <div class="description-content">
                            <div class="g-3 row pt-3 d-flex justify-content-center">

                            @foreach($p_detail as $pdetail)
                                @if($pdetail->type === 'PRODUK-DETAIL' || $pdetail->type === 'SUASANA')
                                    <div class='col-lg-4 col-md-6 col-12 mt-3'>
                                        <img class='img-fluid' src='https://asset.olymplast.co.id/{{$pdetail->path}}/{{$pdetail->filename_webp}}'>
                                    </div>
                                    @php
                                        $found = true; // Set found to true if at least one image is found
                                    @endphp
                                @endif
                            @endforeach

                            @if(!isset($found))
                                @php
                                    for($i = 1; $i <= 13; $i++) {
                                        $external_link = 'https://cdn.olymplast.co.id/'.$detail['path'].'/pd-'.$i.'.webp';
                                        if (@getimagesize($external_link)) {
                                            echo "<div class='col-lg-4 col-md-6 col-12 mt-3'><img class='img-fluid' src='https://cdn.olymplast.co.id/".$detail['path']."/pd-".$i.".webp'></div>";
                                        }
                                    }

                                    for($i = 1; $i <= 13; $i++) {
                                        $external_link = 'https://cdn.olymplast.co.id/'.$detail['path'].'/ss-'.$i.'.webp';
                                        if (@getimagesize($external_link)) {
                                            echo "<div class='col-lg-4 col-md-6 col-12 mt-3'><img class='img-fluid' src='https://cdn.olymplast.co.id/".$detail['path']."/ss-".$i.".webp'></div>";
                                        }
                                    }
                                @endphp
                            @endif
                            </div>
                        </div>
                        <!-- Description Content End -->
                    </div>
                </div>
            </div>
            <!-- Product Details Tabs End -->

        </div>
    </div>

<div class=" slider5 section mt-3">
  <div class="text-center fw-bold h2 section-title">
    Produk Terkait
  </div>
  <div class="container-xxl  position-relative overflow-hidden">
    <div class="px-4">
      <div class="swiper px-3  overflow-hidden"> 
        <div class=" swiper-wrapper position-relative" >
            @foreach($terkait as $new)
            <div class="swiper-slide  py-1">
              <div class="outer-glow rounded overflow-hidden">
                <a href="{{ route('product_detail',['nama'=> $new['nama_slug']]) }}">
                    <div class="terkait">
                        <img class="img-terkait" src="https://asset.olymplast.co.id/{{($new['gambar'])}}" alt="" title="">
                        <div class="p-4">
                            <div class=" text-start">
                            <p class="h6 text-ol-black cat-text text-capitalize">{{$new['category']}}</p>
                            <p class="h5  fw-bold text-ol-black">{{$new['nama']}}</p>
                            </div>
                            <div class="card-border"></div>
                        </div>
                    </div>
                  
                </a>
              </div>
            </div>
            @endforeach
        </div>
        <div class="rounded-end bg-ol-lblue swiper-buttons color-ol-white swiper-button-prev justify-content-center d-flex align-items-center" ><i class="fa-solid fa-angle-left"></i></div>
        <div class="rounded-start bg-ol-lblue swiper-buttons color-ol-white swiper-button-next justify-content-center d-flex align-items-center" ><i class="fa-solid fa-angle-right"></i></div>
      </div>
    </div>
  </div>
</div>

    
    @stop

    @section('script')
    <script>
    var slider5 = new Swiper('.slider5 .swiper', {
        speed: 600,
        slidesPerView: 5,
        spaceBetween: 30,
        loop: true,
        breakpoints: {
            0: {
                slidesPerView: 1.2,
                spaceBetween: 10,
                centeredSlides:true,
            },
            576: {
                slidesPerView: 1.4,
                spaceBetween: 10,
                centeredSlides:true,
            },
            768: {
                slidesPerView: 4,
                spaceBetween: 35,
            },
            992: {
                slidesPerView: 5,
                spaceBetween: 30,
            }
          },
          navigation: {
            nextEl: '.slider5 .swiper-button-next',
            prevEl: '.slider5 .swiper-button-prev',
        },
    }); 
    var galleryThumbs = new Swiper('.details-gallery-thumbs .swiper-container', {
        spaceBetween: 20,
        slidesPerView: 4,
        freeMode: true,
        watchSlidesVisibility: true,
        watchSlidesProgress: true,
        navigation: {
          nextEl: '.details-gallery-thumbs .swiper-button-next',
          prevEl: '.details-gallery-thumbs .swiper-button-prev',
        },
        breakpoints: {          
            0: {
                spaceBetween: 10,
                slidesPerView: 3,
            },
            576: {
                slidesPerView: 4,
            },
        }
      });
      var galleryTop = new Swiper('.details-gallery-images .swiper-container', {
        spaceBetween: 10,     
        thumbs: {
          swiper: galleryThumbs
        }
    });
    function goToPage(numberPage){
     new Swiper('.details-gallery-images .swiper-container').slideTo(getSlideIndexByClass(numberPage))
    };
    function getSlideIndexByClass(className) {
        var index = 0;
        $.each($('.swiper-wrapper').children(), function(i, item) {
            if ($(item).hasClass(className)) {
            index = i;
            return false;
            }
        });
        return index;
    };
    function copyTeks(){
        var valueText = $("#dataCopy").select().val();
        // document.execCommand("copy");
        navigator.clipboard.writeText(valueText);
        alert("url '" + valueText + "' berhasil di salin" )
    }


    </script>
   <script type="application/ld+json">
    {
    "@context": "https://schema.org/",
    "@type": "BreadcrumbList",
    "itemListElement": [
            {
            "@type": "ListItem",
            "position": "1",
            "name": "Home",
            "item": "https://olymplast.co.id/"
            },
            {
            "@type": "ListItem",
            "position": "2",
            "name": "Product",
            "item": "https://olymplast.co.id/product/"
            },
            {
            "@type": "ListItem",
            "position": "3",
            "name": "{{$detail['nama']}}",
            "item": "https://olymplast.co.id/blog/{{$detail['slug']}}"
            }
        ]
    }
    </script>
    <script type="application/ld+json">
    {
        "@type": "Organization",
        "@id": "https://olymplast.co.id/#organization",
        "name": "PT. Cahaya Bintang Plastindo",
        "url": "https://olymplast.co.id/#organization",
        "sameAs": [
            "https://www.facebook.com/olymplast",
            "https://www.instagram.com/olymplast",
            "https://www.youtube.com/c/olympicfurnituregroup_indonesia",
            "https://www.linkedin.com/company/olympic-group-pt-graha-multi-bintang/"
        ],
        "logo": {
            "@type": "ImageObject",
            "@id": "https://olymplast.co.id/#logo",
            "inLanguage": "id-id",
            "url": "https://asset.olymplast.co.id/BRAND/OLYMPLAST/fav_Icon_Olymplast.webp",
            "width": 300,
            "height": 186,
            "caption": "PT. Cahaya Bintang Plastindo"
        },
        "image": {
            "@id": "hhttps://olymplast.co.id/#logo"
        }
    }
    </script>
    @stop