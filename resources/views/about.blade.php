@extends('template')

@section('m_title')
{{$meta->meta_title}}
@stop

@section('m_desc')
{{$meta->meta_desc}}
@stop

@section('m_type')
website
@stop

@section('m_author')
Olymplast
@stop

@section('m_keywords')
{{$meta->keyword}}
@stop

@section('m_image')
https://asset.olymplast.co.id/BRAND/OLYMPIC/ASSET/og-furniture.png
@stop

@section('m_canonical')
https://olymplast.co.id/about
@stop

@section('m_robots')
all
@stop


@section('content')
<div class="kategori section pt-5 mt-5">
  <div class="text-center fw-bold h2 section-title">
    Tentang Olymplast
  </div>
  <div class="container-xxl position-relative">
    <div class="row  px-lg-5 px-2 g-5">
        <div class="col-lg-6 ">
            <img class="rounded overflow-hidden img-fluid w-100" src="https://asset.olymplast.co.id/IMAGES/olymplast-about.jpg" alt="gallery">
        </div>
        <div class="col-lg-6">
            <p class="fs-5 text-ol-grey fw-normal">OLYMPLAST yang hadir sebagai solusi kebutuhan perabot rumah tangga dengan bahan baku plastik untuk seluruh keluarga Indonesia diproduksi oleh PT. Cahaya Bintang Plastindo.</p>
            
            <p class="fs-5 text-ol-grey fw-normal">Perusahaan ini didirikan pada tahun 2015 di kota Gresik dan melakukan ekspansi pada tahun 2018 ke kabupaten Lamongan dengan jumlah karyawan mencapai lebih dari 1,000 orang.</p>
                        
            <p class="fs-5 text-ol-grey fw-normal">PT. Cahaya Bintang Plastindo, merupakan anak perusahaan dari PT. Graha Multi Bintang sebagai holding company yang memegang merek-merek furniture ternama Nasional, sebagai bagian dari Olympic Furniture Group.</p>
                        
            <p class="fs-5 text-ol-grey fw-normal">Bagi Olymplast, rumah merupakan tempat ternyaman bagi setiap anggota keluarga dalam menghabiskan waktu nya. Tempat berbagi cinta, tempat melepaskan lelah, dan tempat yang selalu dirindukan sehingga menjadi perhatian dan prioritas kami.</p>
                        
            <p class="fs-5 text-ol-grey fw-normal">Kami berkomitmen dan memiliki visi untuk selalu menyediakan produk terbaik dengan konsep modern yang mempercantik rumah dan memberi manfaat keluarga Indonesia dengan perabot plastik yang berkelas dan berkualitas.
            
            </p>
            </div>
        </div>
    </div> 
</div>
<div class="kategori section">
  <div class="text-center fw-bold h2 section-title">
    Komitmen kami
  </div>
  <div class="container-xxl position-relative pb-5">
    <div class="row px-lg-4 px-2" style="min-height:370px;">
        <div class="col-lg-3 px-3 py-2">
            <div class="outer-glow rounded p-4 text-center h-100 pt-5">
                <img class="rounded overflow-hidden img-fluid" src="https://asset.olymplast.co.id/IMAGES/olymplast-material.png" alt="gallery">
                <h4 class="pb-1 pt-2">Kualitas Material</h4>
                <p class="text-ol-grey fw-normal fs-5">Olymplast menggunakan material pilihan terbaik untuk setiap produk yang dihasilkan</p>
            </div>
        </div>
        <div class="col-lg-3 px-3 py-2">
            <div class="outer-glow rounded p-4 text-center h-100 pt-5">
            <img class="rounded overflow-hidden img-fluid" src="https://asset.olymplast.co.id/IMAGES/olymplast-desain.png" alt="gallery">
            <h4 class="pb-1 pt-2">Kualitas Desain</h4>
            <p class="text-ol-grey fw-normal fs-5">Olymplast selalu mengembangkan desain dan inovasi terkini sesuai dengan kebutuhan konsumen</p>
            </div>
        </div>
        <div class="col-lg-3 px-3 py-2">
            <div class="outer-glow rounded p-4 text-center h-100 pt-5">
            <img class="rounded overflow-hidden img-fluid" src="https://asset.olymplast.co.id/IMAGES/olymplast-fungsi.png" alt="gallery">
            <h4 class="pb-1 pt-2">Kualitas Fungsi</h4>
            <p class="text-ol-grey fw-normal fs-5">Semua produk Olymplast yang dihasilkan memiliki nilai fungsional tinggi untuk kepuasaan konsumen</p>
            </div>

        </div>
        <div class="col-lg-3 px-3 py-2">
            <div class="outer-glow rounded p-4 text-center h-100 pt-5">
            <img class="rounded overflow-hidden img-fluid" src="https://asset.olymplast.co.id/IMAGES/olymplast-tahan-lama.png" alt="gallery">
            <h4 class="pb-1 pt-2">Kualitas Tahan lama</h4>
            <p class="text-ol-grey fw-normal fs-5">Olymplast fokus kepada uji standard dan kontrol yang ketat agar menciptakan produk yang tahan lama</p>
            </div>

        </div>
        
    </div> 
</div>


   
   

    @stop

    @section('script')
    

    <script type="application/ld+json">
    {
    "@context": "https://schema.org/",
    "@type": "BreadcrumbList",
    "itemListElement": [
            {
            "@type": "ListItem",
            "position": "1",
            "name": "Home",
            "item": "https://olymplast.co.id/"
            },
            {
            "@type": "ListItem",
            "position": "2",
            "name": "About",
            "item": "https://olymplast.co.id/about/"
            }
        ]
    }
    </script>
    <script type="application/ld+json">
    {
        "@type": "Organization",
        "@id": "https://olymplast.co.id/#organization",
        "name": "PT. Cahaya Bintang Plastindo",
        "url": "https://olymplast.co.id/#organization",
        "sameAs": [
            "https://www.facebook.com/olymplast",
            "https://www.instagram.com/olymplast",
            "https://www.youtube.com/c/olympicfurnituregroup_indonesia",
            "https://www.linkedin.com/company/olympic-group-pt-graha-multi-bintang/"
        ],
        "logo": {
            "@type": "ImageObject",
            "@id": "https://olymplast.co.id/#logo",
            "inLanguage": "id-id",
            "url": "https://asset.olymplast.co.id/BRAND/OLYMPLAST/fav_Icon_Olymplast.webp",
            "width": 300,
            "height": 186,
            "caption": "PT. Cahaya Bintang Plastindo"
        },
        "image": {
            "@id": "hhttps://olymplast.co.id/#logo"
        }
    }
    </script>

        @endsection