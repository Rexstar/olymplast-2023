@extends('template')

@section('m_title')
{{$meta->meta_title}}
@stop

@section('m_desc')
{{$meta->meta_desc}}
@stop

@section('m_type')
{{$meta->seo_type}}
@stop

@section('m_author')
Olymplast
@stop

@section('m_keywords')
{{$meta->keyword}}
@stop

@section('m_image')
https://asset.olymplast.co.id/{{$meta->seo_6}}
@stop

@section('m_canonical')
https://olymplast.co.id/category/{{$meta->slug}}
@stop

@section('m_robots')
all
@stop



@section('content')

<div class="section section-padding pt-5 mt-5">
    <div class="container-xxl">
        
        <div class="row flex-row-reverse pt-5 px-lg-4 px-2">
            <div class="col-lg-10">
                <!-- Shop top Bar Start -->
                <div class="shop-top-bar row">

                    <div class="shop-text col ">
                        <p class="h6 text-ol-grey">Menampilkan
                            <span class="fw-bold text-ol-black">{{ $produk->total() }}</span>
                            produk untuk <span class="fw-bold d-lg-inline d-none text-capitalize text-ol-black">{{ $req }}</span>
                            <button
                                class="btn btn-primary btn-small d-lg-none d-inline"
                                type="button"
                                data-bs-toggle="collapse"
                                data-bs-target="#collapseExample"
                                aria-expanded="false"
                                aria-controls="collapseExample">{{ $req }}
                                &nbsp;
                                <i class="fa-solid fa-filter"></i>
                            </button>
                        </p>
                        </div>
                        <div class="collapse pb-4 d-lg-none" id="collapseExample">
                            <div class="outer-glow-sm rounded overflow-hidden p-4">
                                <div class="row">
                                    <div class="col-6 py-2 h6">
                                        <a class="text-ol-grey" href="{{url('product')}}">Semua Produk</a>
                                    </div>
                                    @foreach($all_kategori as $allcat)

                                    <div class="col-6 py-2 h6">
                                    <a class="text-ol-grey" href="{{url('category')}}/{{$allcat->slug}}">{{$allcat->name}}</a>
                                    </div>
                                    @endforeach

                                </div>
                            </div>
                        </div>
                    

                </div>
                <!-- Shop top Bar End -->
                <div class="tab-content">
                    <div class="tab-pane fade show active" id="grid">
                        <!-- Shop Product Wrapper Start -->
                        <div class="shop-product-wrapper">
                            <div class="row g-4 ps-lg-5 ps-0">
                                @foreach($produk as $prod)
                                <div class="col-lg-4 col-6 ">
                                    <div class="outer-glow-sm rounded overflow-hidden">
                                        <div class="">
                                            <!-- <a href="{{url('product/')}}/{{$prod->slug}}">
                                            <div class="h-list-produk">
                                                <img class="img-fluid" src="https://asset.olymplast.co.id/{{$prod->images}}" alt="product">
                                            </div>
                                            <div class="product-content align-middle">
                                                <p class="h5 title text-ol-grey judul-produk">{{$prod->judul_brand}}</p>
                                            </div>
                                            </a> -->
                                            <a href="{{url('product')}}/{{$prod->slug}}">
                                            <div class="single-product d-flex justify-content-center">
                                                <img class="h-100" src="https://asset.olymplast.co.id/{{$prod->images}}" alt="" title="">
                                            </div>
                                            <div class="px-4 pb-4">
                                                <div class=" text-start">
                                                <p class="h6 text-ol-black cat-text">{{($prod->category_name)}}</p>
                                                <p class="h5  fw-bold text-ol-black">{{($prod->judul_brand)}}</p>
                                                </div>
                                                <div class="card-border"></div>
                                            </div>
                                            </a>
                                            <!-- <ul class="product-meta">
                                                <li><a class="action" data-bs-toggle="modal" class="detail-prod" value="{{$prod->id}}" data-bs-target="#quickView" href="#"><i class="pe-7s-look"></i></a></li>
                                            </ul> -->
                                        </div>
                                    </div>
                                </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>
                <div class="page-pagination py-3">
                    {{$produk->links()}}
                </div>
                <!-- Page pagination End -->
            </div>
            <div class="col-lg-2 d-none d-lg-block">
                <!-- Sidebar Start -->
                <div class="sidebar">
                    <div class="sidebar-widget text-start">
                        <h4 class="widget-title text-ol-dgrey">Kategori</h4>
                        <div class=" ">
                                <div class=" h6 pt-2"><a class=" {{ (request()->is('product')) ? 'fw-bold text-ol-black' : 'text-ol-grey' }}" href="{{url('product')}}">Semua</a></div>
                            @foreach($all_kategori as $cat)
                                <div class=" h6 pt-2"><a class=" {{ (request()->is('category/'.$cat->slug.'')) ? 'fw-bold text-ol-black' : 'text-ol-grey' }}" href="{{url('category')}}/{{$cat->slug}}">{{$cat->name}}</a></div>
                            @endforeach
                        </div>
                    </div>
                </div>
                <!-- Sidebar End -->
            </div>
        </div>
    </div>
</div>


    @endsection

    @section('script')
    <script src="{{asset('js/shave.js')}}"></script>


    <script>
        var collapseElementList = [].slice.call(document.querySelectorAll('.collapse'))
        var collapseList = collapseElementList.map(function (collapseEl) {
        return new bootstrap.Collapse(collapseEl)
        });
        let id = this.value;
        link = '<?= url("/product/api/") ?>'+'/'+id;

            $(".detail-prod").keyup(function() {
                let id = $(this).val();
                $.ajax({
                    url: "<?= url('/product/api/') ?>" + '/' + id,
                    method: 'GET',
                    success: function (data) {
                        $("#testtt").val(data);
                    }
                });
            });
            shave('.judul-produk', 70)
    </script>
    @if($req === 'Semua Produk')
    <script type="application/ld+json">
        {
        "@context": "https://schema.org/",
        "@type": "BreadcrumbList",
        "itemListElement": [
                {
                "@type": "ListItem",
                "position": "1",
                "name": "Home",
                "item": "https://olymplast.co.id/"
                },
                {
                "@type": "ListItem",
                "position": "2",
                "name": "Product",
                "item": "https://olymplast.co.id/product/"
                }
            ]
        }
    </script>
    @else
    <script type="application/ld+json">
        {
        "@context": "https://schema.org/",
        "@type": "BreadcrumbList",
        "itemListElement": [
                {
                "@type": "ListItem",
                "position": "1",
                "name": "Home",
                "item": "https://olymplast.co.id/"
                },
                {
                "@type": "ListItem",
                "position": "2",
                "name": "Product",
                "item": "https://olymplast.co.id/product/"
                },
                {
                "@type": "ListItem",
                "position": "3",
                "name": "{{$meta->name}}",
                "item": "https://olymplast.co.id/category/{{$meta->slug}}"
                }
            ]
        }
    </script>
    @endif
    <script type="application/ld+json">
    {
        "@type": "Organization",
        "@id": "https://olymplast.co.id/#organization",
        "name": "PT. Cahaya Bintang Plastindo",
        "url": "https://olymplast.co.id/#organization",
        "sameAs": [
            "https://www.facebook.com/olymplast",
            "https://www.instagram.com/olymplast",
            "https://www.youtube.com/c/olympicfurnituregroup_indonesia",
            "https://www.linkedin.com/company/olympic-group-pt-graha-multi-bintang/"
        ],
        "logo": {
            "@type": "ImageObject",
            "@id": "https://olymplast.co.id/#logo",
            "inLanguage": "id-id",
            "url": "https://asset.olymplast.co.id/BRAND/OLYMPLAST/fav_Icon_Olymplast.webp",
            "width": 300,
            "height": 186,
            "caption": "PT. Cahaya Bintang Plastindo"
        },
        "image": {
            "@id": "hhttps://olymplast.co.id/#logo"
        }
    }
    </script>
    @endsection