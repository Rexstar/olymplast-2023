@extends('template')
@section('content')
<div class="swiper-container">
			    <div class="swiper-wrapper">
			      <div class="swiper-slide"><img class="img-fluid" src="https://asset.olymplast.co.id/BANNER/banner-osb.webp" alt="" title=""></div>
			      <div class="swiper-slide"><img class="img-fluid" src="https://asset.olymplast.co.id/BANNER/banner-osb.webp" alt="" title=""></div>
			      <div class="swiper-slide"><img class="img-fluid" src="https://asset.olymplast.co.id/BANNER/banner-osb.webp" alt="" title=""></div>
			      <div class="swiper-slide"><img class="img-fluid" src="https://asset.olymplast.co.id/BANNER/banner-osb.webp" alt="" title=""></div>
			      <div class="swiper-slide"><img class="img-fluid" src="https://asset.olymplast.co.id/BANNER/banner-osb.webp" alt="" title=""></div>
			      <div class="swiper-slide"><img class="img-fluid" src="https://asset.olymplast.co.id/BANNER/banner-osb.webp" alt="" title=""></div>
			      <div class="swiper-slide"><img class="img-fluid" src="https://asset.olymplast.co.id/BANNER/banner-osb.webp" alt="" title=""></div>
			      <div class="swiper-slide"><img class="img-fluid" src="https://asset.olymplast.co.id/BANNER/banner-osb.webp" alt="" title=""></div>
			      <div class="swiper-slide"><img class="img-fluid" src="https://asset.olymplast.co.id/BANNER/banner-osb.webp" alt="" title=""></div>
			      <div class="swiper-slide"><img class="img-fluid" src="https://asset.olymplast.co.id/BANNER/banner-osb.webp" alt="" title=""></div>
			    </div>
			    <!-- Add Pagination -->
			    <div class="swiper-pagination"></div>
			  </div>
              @stop

@section('script')

    <script>

              var swiper = new Swiper('.swiper-container', {
    	slidesPerView: 2,
      slidesPerColumn: 2,
      spaceBetween: 30,
      slidesPerColumnFill: 'column',
   
		pagination: {
        	el: '.swiper-pagination',
      	},
    });

    </script>
    @endsection