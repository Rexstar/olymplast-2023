<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <title>@yield('m_title')</title>
        <meta name="theme-color" content="#ffffff">
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
        <meta name="author" content="@yield('m_author')">
        <meta name="keywords" content="@yield('m_keywords')">
        <meta name="viewport" content="width=device-width; initial-scale=1.0;"/>
        <meta name="description" content="@yield('m_desc')">
        <meta name="robots" content="@yield('m_robots')"/>
        <meta name="og:title" property="og:title" content="@yield('m_title')">
        <meta name="og:type" property="og:type" content="@yield('m_type')"/>
        <meta name="og:url" property="og:url" content="{{url()->full()}}">
        <meta
            name="og:description"
            property="og:description"
            content="@yield('m_desc')">
        <meta name="og:image" property="og:image" content="@yield('m_image')"/>
        <link rel="canonical" href="@yield('m_canonical')"/>

        <meta name="twitter:card" content="@yield('tw_card')">
        <meta name="twitter:site" content="@OlympicFurnitur">
        <meta name="twitter:creator" content="@OlympicFurnitur">
        <meta name="twitter:title" content="@yield('m_title')">
        <meta name="twitter:description" content="@yield('m_desc')">
        <meta name="twitter:image" content="@yield('m_image')">
        
        <!-- Google Tag Manager -->
        <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
        new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],wokee
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
        })(window,document,'script','dataLayer','GTM-PVF2J2ZW');</script>
        <!-- End Google Tag Manager -->

        <!-- Favicon -->
        <link
            rel="icon"
            href="https://asset.olymplast.co.id/BRAND/OLYMPLAST/fav_Icon_Olymplast.webp"
            type="image/x-icon">

        <link rel="stylesheet" href="{{url('css/style.min.css')}}">
        <!-- <link rel="stylesheet"
        href="{{asset('css/plugins/pe-icon-7-stroke.css')}}"> -->

        <!-- <link rel="stylesheet" href="{{asset('css/all.css')}}"> -->
        <!-- <link rel="stylesheet" href="{{asset('css/app.css')}}"> -->

        <!-- <link rel="stylesheet" href="{{url('css/custom.css')}}"> -->
        <link rel="stylesheet" href="{{url('bootstrap/css/bootstrap.css')}}">
        <!-- <link rel="stylesheet" href="{{url('fontawesome/css/all.css')}}"> -->
        <!-- <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/swiper@10/swiper-bundle.min.css"/> -->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/Swiper/4.5.0/css/swiper.min.css">

        <!-- <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/swiper@11/swiper-bundle.min.css" /> -->
        <!-- <link rel="stylesheet" href="https://unpkg.com/swiper/swiper-bundle.min.css" /> -->


        


    </head>


    <body>
        <!-- Google Tag Manager (noscript) -->
        <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-PVF2J2ZW"
        height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
        <!-- End Google Tag Manager (noscript) -->
        <div class="header top">
            <div class="sticky-header">
                <div class="header-container container-xxl px-lg-5 px-3">
                    <div class="row d-flex justify-content-between">

                        <div class="header-logo col-4 border-r">
                            <div class="pe-lg-2">
                                <a href="{{url('/')}}">
                                    <img class="img-fluid fill-white" src="https://asset.olymplast.co.id/BRAND/OLYMPLAST/fav_Icon_Olymplast.webp">
                                </a>
                            </div>
                            
                        </div>
                        <div class="main-menu  col d-none d-lg-flex justify-content-start ps-5">

                            <ul class="d-flex align-items-center">
                                <li>
                                    <a class="{{ (request()->is('/')) ? 'active' : '' }}" href="{{url('/')}}">HOME</a>
                                </li>
                                <li>
                                    <a class="{{ (request()->is('about')) ? 'active' : '' }} " href="{{url('about')}}">TENTANG</a>
                                </li>
                                <li>
                                    <a class="{{ (request()->is('product')) ? 'active' : '' }} {{ (request()->is('product/*')) ? 'active' : '' }} {{ (request()->is('category/*')) ? 'active' : '' }}" href="{{url('product')}}">PRODUK</a>
                                </li>
                                <li>
                                    <a class="{{ (request()->is('blog')) ? 'active' : '' }} {{ (request()->is('blog/*')) ? 'active' : '' }}" href="{{url('blog')}}">BERITA</a>
                                </li>
                                <li>
                                    <a class="{{ (request()->is('contact')) ? 'active' : '' }}" href="{{url('contact')}}">KONTAK</a>
                                </li>
                            </ul>

                        </div>
                        <div class="header-logo border-l d-flex align-items-center ps-5 pe-0 d-lg-block d-none pt-3">
                            <!-- <a class="header-action-btn header-action-search d-flex align-items-center" href="#/"> -->
                                <span><svg data-bs-toggle="modal" data-bs-target="#searchmodal" class="d-inline-block fa fa-search h5 color-ol-grey" xmlns="http://www.w3.org/2000/svg" height="16" width="16" viewBox="0 0 512 512"><path d="M416 208c0 45.9-14.9 88.3-40 122.7L502.6 457.4c12.5 12.5 12.5 32.8 0 45.3s-32.8 12.5-45.3 0L330.7 376c-34.4 25.2-76.8 40-122.7 40C93.1 416 0 322.9 0 208S93.1 0 208 0S416 93.1 416 208zM208 352a144 144 0 1 0 0-288 144 144 0 1 0 0 288z"/></svg></span>
                                

                            <!-- </a> -->
                        </div>
                        <!-- <div class="col-2 header-actions"> <a class="header-action-btn
                        header-action-search d-flex" href="#/"> <i class="fa fa-search"></i> </a> </div>
                        -->
                        <div class="col-2 header-social-actions d-lg-none">
                            <div class="header-actions">
                                <a class="header-action-btn header-action-btn-menu d-flex" href="#/">
                                    <span></span>
                                    <span></span>
                                    <span></span>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        @yield('content')

        <div class="mobile-menu-wrapper">
            <div class="offcanvas-overlay"></div>

            <!-- Mobile Menu Inner Start -->
            <div class="mobile-menu-inner">

                <!-- Button Close Start -->
                <div class="offcanvas-btn-close">
                    <i class="icofont-close-line"></i>
                </div>
                <!-- Button Close End -->

                <!-- Mobile Menu Inner Wrapper Start -->
                <div class="mobile-menu-inner-wrapper">
            <!-- Mobile Menu Start -->
            
            
            <div class="d-flex justify-content-center px-5">
                <img class="ps-3 img-fluid" src="{{asset('https://asset.olymplast.co.id/BRAND/OLYMPLAST/fav_Icon_Olymplast.webp')}}" alt="Logo Olympic Group">
            </div>
                <div class="px-2 text-center pt-5 fw-bold color-ol-black">
                <a class="h2 " href="{{url('/')}}"><p class="{{ (request()->is('/')) ? 'fw-bold' : '' }}">Home</p></a>
                <a class="h2 " href="{{url('about')}}"><p class="{{ (request()->is('about')) ? 'fw-bold' : '' }}">Tentang</p></a>
                <a class="h2 " href="{{url('product')}}"><p class="{{ (request()->is('product')) ? 'fw-bold' : '' }}">Produk</p></a>
                <a class="h2 " href="{{url('blog')}}"><p class="{{ (request()->is('blog')) ? 'fw-bold' : '' }}">Berita</p></a>
                <a class="h2 " href="{{url('contact')}}"><p class="{{ (request()->is('contact')) ? 'fw-bold' : '' }}">Kontak</p></a>
                </div>
             

                <div class="mt-auto bottom-0 pb-5">
                  <a href="mailto:contact@olymplast.co.id">contact@olymplast.co.id</a>
                    <!-- Contact Links Start -->

                    <!-- Contact Links End -->

                    <!-- <div class="header-social"> <ul> <li> <a href="#/"><i class="fa
                    fa-facebook-f"></i></a> </li> <li> <a href="#/"><i class="fa
                    fa-twitter"></i></a> </li> <li> <a href="#/"><i class="fa
                    fa-pinterest-p"></i></a> </li> </ul> </div> -->
                </div>
                



            </div>
                <!-- Mobile Menu Inner Wrapper End -->

            </div>
            <!-- Mobile Menu Inner End -->
        </div>
        <!-- Mobile Menu End -->

        <!-- Offcanvas Search Start -->
        <div class="offcanvas-search">
            <div class="offcanvas-search-inner">

                <!-- Button Close Start -->
                <div class="offcanvas-btn-close">
                    <i class="fa-solid fa-circle-xmark"></i>
                </div>
                <!-- Button Close End -->

                <!-- Offcanvas Search Form Start -->
                <form class="offcanvas-search-form" action="#">
                    <input type="text" placeholder="Search Here" class="offcanvas-search-input" >
                </form>
                <!-- Offcanvas Search Form End -->

            </div>
        </div>
        <div class="modal fade " id="searchmodal"  aria-hidden="true">
            <div class="modal-dialog bg-transparent">
                <div class="modal-content row d-flex bg-transparent border-0">
                    <div class='vh-40'></div>
                    <form action="{{url('search')}}" method="get">
                    <div class="input-group">
                        <input name="produk" type="text" id="inputmodal" class="form-control" placeholder="Masukkan pencarian produk ... " autofocus>  
                        <div class="input-group-prepend" style="margin-left:-10px">
                            <button class="rounded-0 rounded-end btn btn-primary" type="submit"><i class="fa fa-search"></i> Search</button>
                        </div>
                    </div>
                    </form>
                </div>



                    <!-- <div class="col-10">
                        <input class="" type="text" name="name" placeholder="Name*">
                    </div>
                    <div class="col-2">
                        <button type="button" class="btn btn-secondary d-block" data-bs-dismiss="modal"><i class=" fa fa-search h5 color-ol-white"></i></button>
                    </div> -->
                
                <!-- <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    ...
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary">Save changes</button>
                </div> -->
                </div>
            </div>
        </div>

<div class="section footer-section bg-ol-gray vh-50 border-footer pt-5">
    <div class=" d-flex justify-content-center">
        <div class="container-xxl px-lg-5 px-2 d-flex pb-3 border-bottom row">
            <div class="col-lg-2 col-12">
                <div class="footer-widget">
                    <h4 class="footer-widget-title">Informasi</h4>
                    <div class="row  h6">
                        <a class="d-block py-1 col-lg-12 col-6 text-ol-grey" href="{{url('/')}}">Home</a>
                        <a class="d-block py-1 col-lg-12 col-6 text-ol-grey" href="{{url('about')}}">Tentang</a>
                        <a class="d-block py-1 col-lg-12 col-6 text-ol-grey" href="{{url('product')}}">Produk</a>
                        <a class="d-block py-1 col-lg-12 col-6 text-ol-grey" href="{{url('blog')}}">Berita</a>
                        <a class="d-block py-1 col-lg-12 col-6 text-ol-grey" href="{{url('contact')}}">Kontak</a>
                    </div>
                </div>
            </div>
            <div class="col-lg-2 col-12">
                <div class="footer-widget">
                    <h4 class="footer-widget-title">Layanan Kami</h4>
                    <ul class=" list-unstyled lh-2 h6">
                        <li class="">
                            <a class="text-ol-grey" href="http://mitraolympic.co.id/">Menjadi Mitra</a>
                        </li>
                        <li>
                            <a href="{{url('katalog')}}"><img src="https://asset.olymplast.co.id/IMAGES/BUTTON/button-katalog-merah.webp" alt="Download katalog">
                            </a>
                        </li>

                    </ul>
                </div>
            </div>
            <div class="col-lg-3 col-12">
                <div class="footer-widget">
                    <h4 class="footer-widget-title">Lokasi Kami</h4>
                    <ul class=" list-unstyled lh-2 h6">
                        <li class="">
                            <a class="text-ol-grey" href="#">
                                <div class="d-flex">
                                    <div class="pe-2">
                                        <i class="fa-solid fa-envelope"></i>
                                    </div>
                                    <div>contact@olymplast.co.id</div>
                                </div>
                            </a>
                        </li>
                        <li class="">
                            <a class="text-ol-grey" href="#">
                                <div class="d-flex">
                                    <div class="pe-2">
                                        <i class="fa-solid fa-location-dot"></i>
                                    </div>
                                    <div class="flex-fill">Jl. Raya Gresik-Lamongan Km.40 Desa Rejosari, Kec. Deket, Lamongan 62291</div>
                                </div>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="col-lg-5 col-md-4 col-sm-6">
                <!-- Footer Widget Start -->
                <div class="footer-widget">
                    <h4 class="footer-widget-title">Instagram Olymplast</h4>
                    <div class=" swiperig">
                        <div class=" swiper overflow-hidden px-3"> 
                            <div class=" swiper-wrapper position-relative" >
                                @php
                                    $imagePath = public_path('imageIG'); // Sesuaikan dengan path folder Anda
                                    $images = array_filter(scandir($imagePath), function ($file) {
                                        return pathinfo($file, PATHINFO_EXTENSION) === 'webp' &&
                                            strpos($file, '_medium') !== false;
                                    });
                                    $filenames = array_filter(scandir($imagePath), function ($file) {
                                        return pathinfo($file, PATHINFO_EXTENSION) === 'webp' &&
                                            strpos($file, '_medium') !== false;
                                    });
                                    foreach ($images as $image) {
                                        // Menghapus string setelah karakter '_'
                                        $cleanedFileName = explode('_-_', $image)[0];
                                        // Mengganti karakter '+' dengan '/'
                                        $cleanedFileName = str_replace('+', '/', $cleanedFileName);
                                        // Mengganti karakter ';' dengan ':'
                                        $cleanedFileName = str_replace(';', ':', $cleanedFileName);
                                        // Membuat variabel $link
                                        $link = $cleanedFileName;
                                        echo '
                                        <div class="swiper-slide">
                                            <a class="instagram-image" href="' . $link . '"  target="_blank">
                                                <div class="d-flex align-items-end flex-column swiper-slide  rounded-ol overflow-hidden  shadow-ol-sm">
                                                    <div class="single-banner-03 px-2">
                                                        <img src="' . asset("imageIG/$image") . '" alt="IGImage">
                                                    </div>
                                                </div>
                                            </a>
                                        </div>';
                                    }
                                @endphp
                            </div>
                            <!-- <div class="d-lg-none swiper-button-prev"></div>
                            <div class="d-lg-none swiper-button-next"></div> -->
                            
                            
                        </div>
                    </div>
                    <!-- <div class="ig-container ">
                        <div class="swiper-container ">
                            <div class="swiper-wrapper"  >
                                @php
                                    $imagePath = public_path('imageIG'); // Sesuaikan dengan path folder Anda
                                    $images = array_filter(scandir($imagePath), function ($file) {
                                        return pathinfo($file, PATHINFO_EXTENSION) === 'webp' &&
                                            strpos($file, '_medium') !== false;
                                    });
                                    $filenames = array_filter(scandir($imagePath), function ($file) {
                                        return pathinfo($file, PATHINFO_EXTENSION) === 'webp' &&
                                            strpos($file, '_medium') !== false;
                                    });
                                    foreach ($images as $image) {
                                        // Menghapus string setelah karakter '_'
                                        $cleanedFileName = explode('_-_', $image)[0];
                                        // Mengganti karakter '+' dengan '/'
                                        $cleanedFileName = str_replace('+', '/', $cleanedFileName);
                                        // Mengganti karakter ';' dengan ':'
                                        $cleanedFileName = str_replace(';', ':', $cleanedFileName);
                                        // Membuat variabel $link
                                        $link = $cleanedFileName;
                                        echo '
                                        <div class="swiper-slide ig-swiper">
                                            <a class="instagram-image" href="' . $link . '"  target="_blank">
                                                <div class="d-flex align-items-end flex-column swiper-slide  rounded-ol overflow-hidden  shadow-ol-sm">
                                                    <div class="single-banner-03">
                                                        <img src="' . asset("imageIG/$image") . '" alt="Image">
                                                    </div>
                                                </div>
                                            </a>
                                        </div>';
                                    }
                                @endphp
                            </div>
                            <div class="rounded-end bg-ol-lblue swiper-buttons color-ol-white swiper-button-prev justify-content-center d-flex align-items-center" ><i class="fa-solid fa-angle-left"></i></div>
                            <div class="rounded-start bg-ol-lblue swiper-buttons color-ol-white swiper-button-next justify-content-center d-flex align-items-center" ><i class="fa-solid fa-angle-right"></i></div>
                        </div>
                    </div> -->
                </div>
                <!-- Footer Widget End -->
            </div>
        </div>
    </div>
    
    <p class="text-center h6 pt-2 ">© OLYMPLAST. All Right Reserved</p>

</div>

        <script src="{{asset('js/all.js')}}"></script>
        <script src="{{asset('js/instafeed.js')}}"></script>

        <script src="{{asset('bootstrap/js/bootstrap.min.js')}}"></script>
        <!-- <script src="{{asset('fontawesome/js/all.js')}}"></script> -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.7.0/jquery.min.js"></script>
         <script src="https://cdnjs.cloudflare.com/ajax/libs/Swiper/4.5.0/js/swiper.min.js"></script>

        <!-- <script src="https://cdn.jsdelivr.net/npm/swiper@10/swiper-element-bundle.min.js"></script> -->
        <!-- <script src="https://unpkg.com/swiper/swiper-bundle.min.js"></script> -->
        <!-- <script src="https://cdn.jsdelivr.net/npm/swiper@11/swiper-bundle.min.js"></script> -->

        <script>
            window.onscroll = function () {
                scrollFunction()
            };

            function scrollFunction() {
                if (document.body.scrollTop > 50 || document.documentElement.scrollTop > 50) {
                    var element = document.getElementById("header");
                    element
                        .classList
                        .add("header2");
                    element
                        .classList
                        .remove("header1");
                } else {
                    var element = document.getElementById("header");
                    element
                        .classList
                        .add("header1");
                    element
                        .classList
                        .remove("header2");
                }
            }
        </script>
        <script>

            $('.mobile-menu-open').on('click', function () {
                $('.off-canvas-box').addClass('open')
                $('.menu-overlay').addClass('open')
            });

            $('.menu-close').on('click', function () {
                $('.off-canvas-box').removeClass('open')
                $('.menu-overlay').removeClass('open')
            });

            $('.menu-overlay').on('click', function () {
                $('.off-canvas-box').removeClass('open')
                $('.menu-overlay').removeClass('open')
            });

            /*Variables*/
            var $offCanvasNav = $('.canvas-menu'),
                $offCanvasNavSubMenu = $offCanvasNav.find(
                    '.sub-menu, .mega-sub-menu, .menu-item '
                );

            /*Add Toggle Button With Off Canvas Sub Menu*/
            $offCanvasNavSubMenu
                .parent()
                .prepend('<span class="mobile-menu-expand"></span>');

            /*Close Off Canvas Sub Menu*/
            $offCanvasNavSubMenu.slideUp();

            /*Category Sub Menu Toggle*/
            $offCanvasNav.on(
                'click',
                'li a, li .mobile-menu-expand, li .menu-title',
                function (e) {
                    var $this = $(this);
                    if (($this.parent().attr('class').match(/\b(menu-item-has-children|has-children|has-sub-menu)\b/)) && ($this.attr('href') === '#' || $this.hasClass('mobile-menu-expand'))) {
                        e.preventDefault();
                        if ($this.siblings('ul:visible').length) {
                            $this
                                .parent('li')
                                .removeClass('active-expand');
                            $this
                                .siblings('ul')
                                .slideUp();
                        } else {
                            $this
                                .parent('li')
                                .addClass('active-expand');
                            $this
                                .closest('li')
                                .siblings('li')
                                .find('ul:visible')
                                .slideUp();
                            $this
                                .closest('li')
                                .siblings('li')
                                .removeClass('active-expand');
                            $this
                                .siblings('ul')
                                .slideDown();
                        }
                    }
                }
            );

            $(".sub-menu, .mega-sub-menu, .menu-item")
                .parent("li")
                .addClass("menu-item-has-children");
            $(".mega-sub-menu")
                .parent("li")
                .css("position", "static");

            $(window).on('scroll', function () {
                if ($(this).scrollTop() > 0) {
                    $('.sticky-header').addClass('sticky');
                } else {
                    $('.sticky-header').removeClass('sticky');
                }
            });

            /*--
      Custom script to call Background
      Image & Color from html data attribute
  -----------------------------------*/
            $('[data-bg-image]').each(function () {
                var $this = $(this),
                    $image = $this.data('bg-image');
                $this.css('background-image', 'url(' + $image + ')');
            });
            $('[data-bg-color]').each(function () {
                var $this = $(this),
                    $color = $this.data('bg-color');
                $this.css('background-color', $color);
            });

            /*-----------------------------------------
    Off Canvas Mobile Menu
  -------------------------------------------*/
            $(".header-action-btn-menu").on('click', function () {
                $(".header-action-btn-menu").addClass('open');
                $(".mobile-menu-wrapper").addClass('open');
            });

            $(".offcanvas-btn-close,.offcanvas-overlay").on('click', function () {
                $(".header-action-btn-menu").removeClass('open');
                $(".mobile-menu-wrapper").removeClass('open');
            });

            /*-----------------------------------------
		Off Canvas Search
	-------------------------------------------*/

            $(".header-action-search").on('click', function () {
                $("body").addClass('fix');
                $(".offcanvas-search").addClass('open');
            });

            $(".offcanvas-btn-close,.body-overlay").on('click', function () {
                $("body").removeClass('fix');
                $(".offcanvas-search").removeClass('open');
            });

            /*----------------------------------------
    Responsive Mobile Menu
  ------------------------------------------*/
            //Variables
            var $offCanvasNav = $('.mobile-menu, .category-menu'),
                $offCanvasNavSubMenu = $offCanvasNav.find('.dropdown');

            //Close Off Canvas Sub Menu
            $offCanvasNavSubMenu.slideUp();

            //Category Sub Menu Toggle
            $offCanvasNav.on('click', 'li a, li .menu-expand', function (e) {
                var $this = $(this);
                if (($this.parent().attr('class').match(/\b(menu-item-has-children|has-children|has-sub-menu)\b/)) && ($this.attr('href') === '#' || $this.hasClass('menu-expand'))) {
                    e.preventDefault();
                    if ($this.siblings('ul:visible').length) {
                        $this
                            .parent('li')
                            .removeClass('active');
                        $this
                            .siblings('ul')
                            .slideUp();
                    } else {
                        $this
                            .parent('li')
                            .addClass('active');
                        $this
                            .closest('li')
                            .siblings('li')
                            .removeClass('active')
                            .find('li')
                            .removeClass('active');
                        $this
                            .closest('li')
                            .siblings('li')
                            .find('ul:visible')
                            .slideUp();
                        $this
                            .siblings('ul')
                            .slideDown();
                    }
                }
            });

            /*----------------------------------------*/
            /*  When document is loading, do
  /*----------------------------------------*/
            var varWindow = $(window);
            varWindow.on('load', function () {
                AOS.init({once: true});
            });

            /*----------------------------------------*/
            /*  Project Filter
  /*----------------------------------------*/
            $('.gallery-filter-nav').on('click', 'button', function () {
                var filterValue = $(this).attr('data-filter');
                $grid.isotope({filter: filterValue});

                $(this)
                    .siblings('.active')
                    .removeClass('active');
                $(this).addClass('active');
            });

            var $grid = $('.gallery-filter-wrapper').isotope(
                {itemSelector: '.filter-item', percentPosition: true}
            );

            $('#searchmodal').on('shown.bs.modal', function() {
                setTimeout(function (){
                    $('#inputmodal').focus();
                }, 1000);
            })

            

        </script>
    <script src="{{asset('js/main.js')}}"></script>

    @yield('script')

    </body>

</html>