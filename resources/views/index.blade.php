@extends('template')

@section('m_title')
{{$meta->meta_title}}
@stop

@section('m_desc')
{{$meta->meta_desc}}
@stop

@section('m_type')
{{$meta->seo_type}}
@stop

@section('m_author')
Olymplast
@stop

@section('m_keywords')
{{$meta->keyword}}
@stop

@section('m_image')
https://asset.olymplast.co.id/BRAND/OLYMPLAST/fav_Icon_Olymplast.webp
@stop

@section('m_canonical')
https://olymplast.co.id
@stop

@section('m_robots')
all
@stop

@section('content')
<div class="blank-header d-block">

</div>


<div class="section main-banner position-relative">
  <div class="swiper shadow">
    <div class="swiper-wrapper">

      @if($windowWidth > 0)
        {{-- If screen width is greater than 600px, load $banner_pc --}}
        @foreach($banner_pc as $b_p)
          <div class="swiper-slide">
            <a href="{{ $b_p->external_link }}">
              <picture>
                <source media="(min-width:1400px)" srcset="https://asset.olymplast.co.id/BANNER/{{ $b_p->image_desktop }}">
                <source media="(min-width:1000px)" srcset="https://asset.olymplast.co.id/BANNER/{{ str_replace('.webp', '-medium.webp', $b_p->image_desktop) }}">
                <img class="img-fluid" src="https://asset.olymplast.co.id/BANNER/{{ str_replace('.webp', '-small.webp', $b_p->image_desktop) }}" alt="{{ $b_p->alt_text }}" title="{{ $b_p->title }}">
              </picture>
            </a>
          </div>
        @endforeach

      @else
        {{-- If screen width is less than or equal to 600px, load $banner_mobile --}}
        @foreach($banner_mobile as $b_m)
          <div class="swiper-slide ">
            <a href="{{ $b_m->external_link }}">
              <!-- Image for mobile -->
              <img class="img-fluid" src="https://asset.olymplast.co.id/BANNER/{{ $b_m->image_mobile }}" alt="{{ $b_m->alt_text }}" title="{{ $b_m->title }}">
            </a>
          </div>
        @endforeach

      @endif

    </div>
      <div style="" class="bottom-0 swiper-pagination"></div>
  </div>
</div>

<!-- <div class="section main-banner position-relative ">
  <div class="swiper shadow">
    <div class="swiper-wrapper ">
      @foreach($banner_pc as $b_p)
        <div class="swiper-slide d-lg-block d-none">
          <a href="{{ ($b_p->external_link) }}">
            <picture>
              <source media="(min-width:1400px)" srcset="https://asset.olymplast.co.id/BANNER/{{($b_p->image_desktop)}}">
              <source media="(min-width:1000px)" srcset="https://asset.olymplast.co.id/BANNER/{{(str_replace('.webp','-medium.webp',$b_p->image_desktop))}}">
              <img class="img-fluid" src="https://asset.olymplast.co.id/BANNER/{{(str_replace('.webp','-small.webp',$b_p->image_desktop))}}" alt="{{($b_p->alt_text)}}" title="{{($b_p->title)}}">
            </picture>
            </a>
        </div>
      @endforeach 
      @foreach($banner_mobile as $b_m)
        <div class="swiper-slide d-block d-lg-none">
          <a href="{{ $b_m->external_link }}">
            <img class="img-fluid" src="https://asset.olymplast.co.id/BANNER/{{ $b_m->image_mobile }}" alt="{{ $b_m->alt_text }}" title="{{ $b_m->title }}">
          </a>
        </div>
      @endforeach
    </div>
    <div class="swiper-pagination"></div>
  </div>
</div> -->
<!-- <div class="section main-banner-m position-relative d-block d-lg-none">
  <div class="swiper shadow">
    <div class="swiper-wrapper ">
      @foreach($banner_mobile as $b_m)
        <div class="swiper-slide d-block d-lg-none">
          <a href="{{ ($b_m->external_link) }}">
            <img class="img-fluid " src="https://asset.olymplast.co.id/BANNER/{{($b_m->image_mobile)}}" alt="{{($b_p->alt_text)}}" title="{{($b_p->title)}}">
          </a>
        </div>
      @endforeach
    </div>
  </div>
</div> -->

<div class="kategori section">
  <div class="text-center fw-bold h2 section-title">
    Produk Kami
  </div>
  <div class="container-xxl position-relative">
    <div class=" px-lg-4 px-2">
      <div class="swiper-container px-lg-3 px-1">
      <div class="swiper-wrapper ">
        @foreach($kategori as $kat)
        <div class="swiper-slide h-100p  rounded shadow-sm">
        <a href="{{url('category')}}/{{$kat->slug}}">
          <div class="col-12 bg-ol-grey rounded overflow-hidden h-lg-100  position-relative cat-card">
            <div class="position-absolute top-0  p-lg-5 cat-image d-lg-block d-none">
              <!-- <img class="img-fluid" style="width:300px" src="https://asset.olymplast.co.id/{{$kat->image}}" alt="" title=""> -->
              <picture>
                <source media="(min-width:1400px)" srcset="https://asset.olymplast.co.id/{{$kat->image}}">
                <source media="(min-width:1000px)" srcset="https://asset.olymplast.co.id/{{(str_replace('.webp','-medium.webp',$kat->image))}}">
                <img class="img-fluid" src="https://asset.olymplast.co.id/{{(str_replace('.webp','-small.webp',$kat->image))}}" alt="{{($kat->image_alt)}}" title="{{($kat->image_title)}}">
              </picture>
            </div>
            <div class="position-absolute top-0 end-0 p-5 mt-3 me-2 cat-border">
            
            </div>
            <div class='d-lg-none d-block pt-3'>
              <img class="img-fluid" style="width:300px" src="https://asset.olymplast.co.id/{{(str_replace('.webp','-small.webp',$kat->image))}}" alt="{{($kat->image_alt)}}" title="{{($kat->image_title)}}">
            </div>
            <div style="height:40px" class="d-lg-none d-block h6 text-uppercase  col-12 ps-2 text-start d-flex align-items-center">{{$kat->name}}</div>

            <div class="position-absolute bottom-0  d-lg-block d-none px-5 pb-4 text-lg-end text-left">
              <div class="h2 text-uppercase fw-bold cat-title col-lg-7 offset-lg-5 col-12 ps-lg-5 ps-1">{{$kat->name}}</div>
              <div class="col-lg-6 offset-lg-6 cat-text d-lg-block d-none"><p class="lh-sm ">{{$kat->content}}</p></div>
              <!-- <button type="button" class="btn btn-primary btn-sm">Lihat Selengkapnya</button> -->
              <div class="d-lg-block d-none">
                <a href="{{url('category')}}/{{$kat->slug}}" class="btn btn-primary btn-sm">Lihat Selengkapnya</a>
              </div>
              </div>
            
          </div>
        </a>
        </div>
        @endforeach
      </div>
      </div>
      
    </div>
        <div class="rounded-end bg-ol-lblue swiper-buttons color-ol-white swiper-button-prev justify-content-center d-flex align-items-center" ><i class="fa-solid fa-angle-left"></i></div>
        <div class="rounded-start bg-ol-lblue swiper-buttons color-ol-white swiper-button-next justify-content-center d-flex align-items-center" ><i class="fa-solid fa-angle-right"></i></div>
  </div> 
</div>
<div class="section">
  <div class="text-center fw-bold h2 section-title">
    Produk Terlaris
  </div>
  <div class="container-xxl px-lg-5  position-relative ">
    <div class="overflow-hidden slider5">
      <div class=" swiper px-1"> 
        <div class=" swiper-wrapper position-relative" >
            @foreach($terbaru as $new)
            <div class="swiper-slide  py-1">
              <div class="outer-glow rounded overflow-hidden">
                <a href="{{url('product')}}/{{$new->slug}}">
                  <img class="img-fluid" src="https://asset.olymplast.co.id/{{($new->images)}}" alt="" title="">
                  <div class="p-4">
                    <div class=" text-start">
                      <p class="h6 text-ol-black cat-text">{{($new->category_name)}}</p>
                      <p class="h5  fw-bold text-ol-black">{{($new->judul_brand)}}</p>
                    </div>
                    <div class="card-border"></div>
                  </div>
                </a>
              </div>
            </div>
            @endforeach
        </div>
        <!-- <div class="d-lg-none swiper-button-prev"></div>
        <div class="d-lg-none swiper-button-next"></div> -->
        <div class="rounded-end bg-ol-lblue swiper-buttons color-ol-white swiper-button-prev justify-content-center d-flex align-items-center" ><i class="fa-solid fa-angle-left"></i></div>
        <div class="rounded-start bg-ol-lblue swiper-buttons color-ol-white swiper-button-next justify-content-center d-flex align-items-center" ><i class="fa-solid fa-angle-right"></i></div>
        
      </div>
    </div>
  </div>
</div>
<div class="section container-xxl px-5 position-relative d-lg-block d-none">
    <div class=" outer-glow rounded overflow-hidden px-0">
      <img class="img-fluid " src="https://asset.olymplast.co.id/BANNER/olymplast-separator.webp" alt="" title="">
    </div>
</div>
<div class=" section ">
  <div class="text-center fw-bold h2 section-title">
    Testimoni Pelanggan
  </div>
  <div class="container-xxl px-lg-5 position-relative overflow-hidden pb-4">
    <div class="row d-flex">
      <div class="col-lg-6 d-lg-block d-none align-items-stretch position-relative d-flex flex-column ">
        <div class="h-100 rounded overflow-hidden">
          <a href="{{url('kol')}}">
            <div class="h-100" style="background: url('https://asset.olymplast.co.id/ENDORSEMENT/{{$first->image_content}}') no-repeat center center; background-size:cover; ">
            </div>
          </a>
        </div>
        <div class="py-4 d-flex">
          <div class="">
            <img class="rounded-img" src="https://asset.olymplast.co.id/ENDORSEMENT/{{$first->image_user}}" alt="" title="">
          </div>
          <div class="flex-grow-1 align-self-center">
          <a href="{{url('name')}}">
            <p class="h5 ps-3 ">{{$first->name}}</p>
          </a>
          </div>
        </div>
        
      </div>
      <div class="col-lg-6 col-12">
        <div class="row">
          @foreach($kol as $kols)
          
          <div class="col-6 shadowhover">
            <div class="rounded overflow-hidden shadow h-75p">
              <!-- <img class='img-fluid' src="https://asset.olymplast.co.id/ENDORSEMENT/{{$kols->image_content}}" alt="" title=""> -->
         <div class="h-100" style="background: url('https://asset.olymplast.co.id/ENDORSEMENT/{{$kols->image_content}}') no-repeat center center; background-size:cover; "></div>

            </div>
            <div class="py-4 d-flex">
              <div class="">
              <a href="{{url('kol')}}">
                <img class='rounded-img' src="https://asset.olymplast.co.id/ENDORSEMENT/{{$kols->image_user}}" alt="" title="">
                </a>
              </div>
              <div class="flex-grow-1 align-self-center">
                <a href="{{url('kol')}}">
                  <p class="h5 ps-3 ">{{($kols->name)}}</p>
                </a>
              </div>
            </div>
          </div>
          
          @endforeach
      </div>
    </div>
  </div>
</div>
<div class=" slider3 section">
  <div class="text-center fw-bold h2 section-title">
    Artikel dan Berita Terbaru
  </div>
  <div class="container-xxl  position-relative overflow-hidden">
    <div class="px-lg-4 px-0">
      <div class="swiper px-5 px-lg-3 overflow-hidden"> 
        <div class=" swiper-wrapper position-relative py-2" >
            @foreach($news as $nws)
            <div class="swiper-slide outer-glow rounded overflow-hidden">
                <a href="{{url('blog/')}}/{{$nws->slug}}">
                  <div class="d-flex flex-column">
                    <div class="h-100 col-12">
                      <img class="img-fluid" src="https://asset.olymplast.co.id/NEWS/{{($nws->image)}}" alt="" title="">
                    </div>
                    <div class="d-flex px-3">
                      <div class=" d-flex pe-2 py-1">
                        <!-- <div class="col flex-grow-1 align-self-center">
                          <i class="fa-solid fa-calendar-days text-ol-grey "></i>
                        </div> -->
                        <div class="col d-flex flex-column  pe-2 align-self-center">
                          <div class="border-end pe-2" >
                            <div class="row " >
                              <span class="h8  text-ol-grey">{{ jdmonthname(date('m', strtotime($nws->date)),0) }}</span>
                            </div>
                            <div class="row">
                              <span class="mb-0 h6 lh-05 text-ol-black">{{ date('j', strtotime($nws->date)) }}</span>
                            </div>
                            <div class="row">
                              <span class="h8  text-ol-grey">{{ date('Y', strtotime($nws->date)) }}</span>
                            </div>
                          </div>
                        </div>
                      </div>
                      
                      <div class="ms-auto flex-grow-1 align-self-center pt-2">
                        <p class="h6 text-ol-black text-start">{{($nws->title)}}</p>
                      </div>
                    </div>
                  </div>
                </a>
            </div>
            @endforeach
        </div>
        <div class="d-lg-none swiper-button-prev"></div>
        <div class="d-lg-none swiper-button-next"></div>
      </div>
    </div>
  </div>
  <div class="text-center fw-bold h2 pb-5">
    <a class='btn btn-light btn-sm border' href="{{url('blog')}}">Lihat Semua</a>

  </div>
</div>

@stop

@section('script')

    <script>
    var swiper = new Swiper('.kategori .swiper-container', {
      spaceBetween: 30,
      slidesPerView: 2,
      slidesPerColumn: 2,
      loop: true,
      slidesPerColumnFill: 'column',
      
      navigation: {
            nextEl: '.kategori .swiper-button-next',
            prevEl: '.kategori .swiper-button-prev',
        },
    });
    var mainslider = new Swiper('.main-banner .swiper', {
        speed: 600,
        effect: "fade",
        autoplay: {
          delay: 6000,
          disableOnInteraction: true,
        },
        loop: true,
        pagination: {
          el: ".swiper-pagination",
      },
    }); 
    var slider5 = new Swiper('.slider5 .swiper', {
        slidesPerView: 5,
        spaceBetween: 10,
        speed: 600,
        loop: true,
        breakpoints: {
            0: {
                slidesPerView: 2,
                spaceBetween: 10,
            },
            576: {
                slidesPerView: 2,
                spaceBetween: 10,
            },
            768: {
                slidesPerView: 4,
                spaceBetween: 20,
            },
            992: {
                slidesPerView: 5,
                spaceBetween: 20,
            }
          },
          navigation: {
            nextEl: '.slider5 .swiper-button-next',
            prevEl: '.slider5 .swiper-button-prev',
        },
    }); 
    var slider3 = new Swiper('.slider3 .swiper', {
        speed: 600,
        loop: true,
        slidesPerView: 3,
        spaceBetween: 25,
        breakpoints: {
            0: {
                slidesPerView: 1,
                spaceBetween: 10,
            },
            576: {
                slidesPerView: 1,
                spaceBetween: 15,
            },
            768: {
                slidesPerView: 2,
                spaceBetween: 20,
            },
            992: {
                slidesPerView: 3,
                spaceBetween: 25,
            },
          },
          navigation: {
            nextEl: '.slider3 .swiper-button-next',
            prevEl: '.slider3 .swiper-button-prev',
        },
    }); 
    
    </script>
    <script type="application/ld+json">
    {
        "@type": "Organization",
        "@id": "https://olymplast.co.id/#organization",
        "name": "PT. Cahaya Bintang Plastindo",
        "url": "https://olymplast.co.id/#organization",
        "sameAs": [
            "https://www.facebook.com/olymplast",
            "https://www.instagram.com/olymplast",
            "https://www.youtube.com/c/olympicfurnituregroup_indonesia",
            "https://www.linkedin.com/company/olympic-group-pt-graha-multi-bintang/"
        ],
        "logo": {
            "@type": "ImageObject",
            "@id": "https://olymplast.co.id/#logo",
            "inLanguage": "id-id",
            "url": "https://asset.olymplast.co.id/BRAND/OLYMPLAST/fav_Icon_Olymplast.webp",
            "width": 300,
            "height": 186,
            "caption": "PT. Cahaya Bintang Plastindo"
        },
        "image": {
            "@id": "hhttps://olymplast.co.id/#logo"
        }
    }
    </script>
    <script type="application/ld+json">
    {
      "@context": "https://schema.org",
      "@type": "BreadcrumbList",
      "itemListElement": [
          {
              "@type": "ListItem",
              "position": 1,
              "item": {
                  "@id": "https://olymplast.co.id/",
                  "name": "Home"
              }
          }
      ]
    }
    </script>

@endsection